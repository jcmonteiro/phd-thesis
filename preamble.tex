% Make the table of contents appear in the PDF bookmarks
\makeatletter
\usepackage{etoolbox}
\pretocmd{\tableofcontents}{%
  \if@openright\cleardoublepage\else\clearpage\fi
  \pdfbookmark[0]{\contentsname}{toc}%
}{}{}%
\makeatother

\usepackage{amsmath,amssymb}
\usepackage{hyperref}

% For frames such as the one used in the \remind
% Setting the frame method to tikz fixes many bugs, such as the
% one where the top line is not drawn
\usepackage[framemethod=tikz]{mdframed}
%
\newmdenv[linecolor=black,backgroundcolor=red!10]{remind}

% For prettier theorems
\usepackage{amsthm,thmtools}
% For referencing commands \cref and \Cref
\usepackage{cleveref}

% Controls the way items are referenced
\usepackage{enumitem}
\setlist[enumerate,1]{label=(\arabic*),ref=(\arabic*)}
\setlist[enumerate,2]{label=(\alph*),ref=(\arabic{enumi}-\alph*)}

% For bold math
\usepackage{bm}

% For subfigure
\usepackage{subcaption}

% Pretty tables with pgfplots
\usepackage{pgfplotstable,colortbl,arydshln,multirow,booktabs,adjustbox}

% Configure hyperref
\hypersetup{
    pdfnewwindow=true,      % links in new PDF window
    colorlinks=true,        % false: boxed links; true: colored links
    linkcolor=blue,         % color of internal links (change box color with linkbordercolor)
    citecolor=blue,         % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=magenta        % color of external links
}

% Configure natbib
%\setcitestyle{round}

% Configure cleveref
\renewcommand*{\ref}[1]{\fref{#1}}

% Configure the graphics path
\graphicspath{figs/}

% Custom commands
\DeclareMathOperator*{\sign}{sign}
\DeclareMathOperator*{\sat}{sat}
\DeclareMathOperator*{\sigmoid}{sigmoid}
\DeclareMathOperator*{\diag}{diag}
\DeclareMathOperator*{\signeps}{\sigmoid_{\displaystyle\epsilon}}

\providecommand*{\floor}[1]{\left\lfloor#1\right\rfloor}
\providecommand*{\mathset}[1]{\mathbb{#1}}
\providecommand*{\naturals}{\mathset{N}}
\providecommand*{\reals}{\mathset{R}}
\providecommand*{\preals}{\reals_+}
\providecommand*{\realsE}{\bar{\reals}}
\providecommand*{\prealsE}{\bar{\reals}_+}
\providecommand*{\integers}{\mathset{Z}}
%
\providecommand*{\classC}[1]{C^{#1}}
\providecommand*{\classK}{\mathcal{K}}
\providecommand*{\classKL}{\classK\mathcal{L}}
\providecommand*{\continuous}{\classC{0}}
\providecommand*{\vecvar}[1]{#1}
\providecommand*{\abs}[1]{\left|#1\right|}
\providecommand*{\norm}[1]{\left| \! \left| #1 \right| \! \right|}
\providecommand*{\order}[1]{\mathcal{O}(#1)}
\providecommand*{\transp}[1]{{#1}^\mathsf{T}}
\providecommand*{\interval}{\mathcal{I}}
\providecommand*{\inner}[2]{< \! #1,#2 \! >}
\providecommand*{\ball}[1]{B(#1)}
\providecommand*{\contingent}[2]{D_{\mathbb{#1}}#2}
\providecommand*{\dcontingent}[4]{D_{\mathbb{#1},\mathbb{#2}(#3)}#4}
\providecommand*{\citep}[1]{\parencite{#1}}
\providecommand*{\derivp}[2]{\dfrac{\partial #1}{\partial #2}}

\providecommand*{\graph}{\mathcal{G}}
\providecommand*{\setlabels}{\mathcal{V}}
\providecommand*{\setedges}{\mathcal{E}}
\providecommand*{\iff}{\Longleftrightarrow}
\providecommand*{\setinputs}{\mathcal{U}}
\providecommand*{\setestimates}{\mathcal{X}}
\providecommand*{\diamgraph}[1][\graph]{d\!\left(#1\right)}

% Table related custom commands
% \newcommand*{\emptycell}{\multicolumn{1}{|c|}{\cellcolor{white}}}
% \newcommand*{\okcell}{$\times$}
\providecommand*{\emptycell}{\cellcolor{white}}
\providecommand*{\okcell}{\includegraphics[width=2mm]{figs/tikz/cell_full}}
\providecommand*{\hfcell}{\includegraphics[width=2mm]{figs/tikz/cell_half}}
\providecommand*{\igcell}{\includegraphics[width=2mm]{figs/tikz/cell_ignore}}
\providecommand*{\doubtc}{?}

% Allow equations to span multiple pages but avoid as mush as possible
\allowdisplaybreaks[1]

% bibliography
\addbibresource{main.bib}
\addbibresource{esc.bib}
\addbibresource{smc.bib}
\addbibresource{optimization.bib}
\addbibresource{consensus.bib}

\newtheorem{example}{\bf{Example}}
\newenvironment{Example}{\begin{example}}{\\*[-1.5ex]\rule{\linewidth}{0.4pt}\end{example}}

\declaretheoremstyle[
    headfont=\normalfont\bfseries,
    bodyfont=\normalfont\itshape,
    spaceabove=2ex
]{sty-thm-default}
\declaretheoremstyle[
    headfont=\normalfont\bfseries,
    bodyfont=\normalfont
]{sty-thm-proof}
%
\declaretheorem[
    style=sty-thm-default,
    name=Theorem,
    refname={Theorem,Theorems},
    Refname={Theorem,Theorems}
]{theorem}
%
\declaretheorem[
    style=sty-thm-default,
    name=Lemma,
    refname={Lemma,Lemmas},
    Refname={Lemmas,Lemmas}
]{lemma}
%
\declaretheorem[
    style=sty-thm-default,
    name=Corollary,
    refname={Corollary,Corollaries},
    Refname={Corollary,Corollaries}
]{corollary}
%
\declaretheorem[
    style=sty-thm-default,
    name=Remark,
    refname={Remark,Remarks},
    Refname={Remark,Remarks}
]{remark}
%
\declaretheorem[
    style=sty-thm-default,
    name=Assumption,
    refname={Assumption,Assumptions},
    Refname={Assumption,Assumptions}
]{assumption}
%
\declaretheorem[
    style=sty-thm-default,
    name=Proposition,
    refname={Proposition,Propositions},
    Refname={Proposition,Propositions}
]{proposition}
%
\declaretheorem[
    style=sty-thm-default,
    name=Conjecture,
    refname={Conjecture,Conjectures},
    Refname={Conjecture,Conjectures}
]{conjecture}
%
\declaretheorem[
    style=sty-thm-default,
    name=Definition,
    refname={Definition,Definitions},
    Refname={Definition,Definitions}
]{definition}

\let\proof\undefined
\def\QEDclosed{\mbox{\rule[0pt]{1.3ex}{1.3ex}}}
\declaretheorem[
    style=sty-thm-proof,
    name=Proof,
    numbered=no,
    qed=\QEDclosed
]{proof}
%
\declaretheorem[
    style=sty-thm-proof,
    name=Sketch of Proof,
    numbered=no,
    qed=\QEDclosed
]{sketch}