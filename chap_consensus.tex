\chapter{Dynamic Estimator for Extremum Consensus}
\label{chap:consensus}
Consensus estimation consists in applying an update rule such that, in a distributed network where each node has its inputs and estimates, every node converges to a desired performance index, which is a function of the network inputs. To converge to this desired index, each node can exchange information with a finite number of neighbor nodes.

%%%%

In distributed optimization problems, it is appealing to consider consensus estimation as a valuable tool to perform scalarization of the multiple objective functions, see \parencite{poveda2013distributed,guay2018distributed,salamah2018cooperative}. In these works, the authors consider the average consensus estimator of \textcite{freeman2006stability} to perform scalarization through weighted sums. In all three works, the final objective is to solve the single-objective optimization problem, obtaining then a Pareto efficient solution. The downside of this approach is that, if the set of Pareto efficient solutions is non-convex, not all solutions are reachable after solving the single-objective optimization problem.

%%%%

To counter this drawback, in this Thesis, we propose a novel dynamic consensus estimator capable of finding the overall maximum (minimum) input in a network. Thus, applying the proposed algorithm in a distributed optimization problem, one may consider the nodes inputs as the individual objective functions, and by estimating the overall maximum (minimum) objective in the network, one may apply scalarization via Chebyshev's distance to find Pareto efficient solutions for classes of optimization problems with non-convex Pareto sets. Naturally, if all objective functions are convex, Chebyshev's distance can still be used to find all Pareto efficient solutions, similarly to how weighted sums would be used.


%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% | SECTION | %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
\section{Problem Formulation}
\label{chap:consensus.formulation}
There are two varieties of consensus estimators: static estimators and dynamic estimators. In \emph{static consensus}, a snapshot of the nodes inputs at a given time is used to initialize the algorithm, but changes to these inputs are ignored thereafter. Static consensus is useful if the inputs vary slowly or do not vary at all. If the inputs vary through time, and such changes cannot be ignored, one should instead consider dynamic consensus. In \emph{dynamic consensus}, algorithms are designed such that each node tracks the overall network performance index as it changes through time due to changes to the nodes inputs. In this section, we provide the mathematical background and the assumptions needed to develop our dynamic consensus estimator for maximum (minimum) estimation.

%%%%

Consider a group of $n$ labeled nodes, with labels belonging to the set $\setlabels = \left\{1,\ 2,\ \hdots\ ,\ n\right\}$, each one holding an input $u_j(t) \in \reals$ and an estimate $x(t) \in \reals$, both function of time. These nodes interact over a communication network, with topology represented by a directed graph $\graph = \left(\setlabels,\ \setedges\right)$, where $\setedges \subset \setlabels \times \setlabels$ is the set of all edges which connect two nodes. It is said that a node $i$ receives information from node $j$ if and only if $(i,j) \in \setedges$. When this is the case, node $i$ has access to the inputs and the estimates of node $j$ at any given time $t$.

%%%%

Many properties can be associated with graphs, but we are particularly interested in the following (see \cref{fig:graph_examples} for illustrations of some of them).

%%%%

\begin{definition}[Undirected Path]
\label{def:consensus.undirected.path}
    An \emph{undirected path} is a sequence of nodes $i_1,i_2,\hdots,i_p$ such that either $(i_j,i_{j+1}) \in \setedges$ or $(i_{j+1},i_j) \in \setedges$.
\end{definition}

\begin{definition}[Weakly Connected]
\label{def:consensus.weakly.connected}
    A graph is \emph{weakly connected} if every pair of nodes lie on some undirected path.
\end{definition}

\begin{definition}[Directed Cycle]
\label{def:consensus.directed.cycle}
    A \emph{directed cycle} is a sequence of nodes $i_1,i_2,\hdots,i_p$, with $i_1 = i_p$, such that $(i_j,i_{j+1}) \in \setedges$.
\end{definition}

\begin{definition}[Strongly Connected]
\label{def:consensus.strongly.connected}
    A graph is \emph{strongly connected} if every pair of nodes lie on some directed cycle. Considering undirected graphs, weakly connected $\iff$ strongly connected.
\end{definition}

\begin{definition}[Directed Path]
\label{def:consensus.directed.path}
    Similar to an undirected path, a \emph{directed path} is a sequence of nodes $i_1,i_2,\hdots,i_p$ such that $(i_j,i_{j+1}) \in \setedges$.
\end{definition}

\begin{definition}[Shortest Path, Distance, Eccentricity, and Diameter]
\label{def:consensus.shortest.distance.eccentricity.diameter}
    The \emph{shortest path} between two nodes in a directed graph is the directed path with the least amount of edges. The \emph{distance} between two nodes $\delta(i,j)$ is the number of edges in a shortest path, and the \emph{eccentricity} of a node is its greatest distance to any other node. The \emph{diameter}, denoted $\diamgraph$, is the greatest eccentricity of all nodes in a graph.
\end{definition}

%%%%

\begin{figure}[h]
    \centering
    \includegraphics[scale=1]{figs/tikz/example_graph}
    \caption{Examples of (a) an undirected strongly connected graph, (b) a weakly connected but not strongly connected directed graph, and (c) a strongly connected directed graph.}
    \label{fig:graph_examples}
\end{figure}

%%%%

\begin{assumption}[Graph Properties]
\label{ass:consensus.graph}
    For the remaining of this chapter, we use $\graph = \left(\setlabels,\ \setedges\right)$ to denote a directed and strongly connected graph, with nodes belonging to $\setlabels = \left\{1,\ 2,\ \hdots\ ,\ n\right\}$ and edges belonging to $\setedges \subset \setlabels \times \setlabels$.
\end{assumption}

%%%%

For a given node $j$,
%
\begin{subequations}
\begin{align}
    \setinputs_j(t) = \left\{ u_i(t) \in \reals : (j,i) \in \setedges \right\} \cup \{u_j(t)\}
    \\
    \setestimates_j(t) = \left\{ x_i(t) \in \reals : (j,i) \in \setedges \right\} \cup \{x_j(t)\}
\end{align}
\end{subequations}
%
are the sets containing all inputs and all estimates that node $j$ is aware of, including its own input and estimate, which belong to the singletons $\{u_j(t)\}$ and $\{x_j(t)\}$.

%%%%

Given a vector $v \in \reals^p$, $v_j \in \reals$ denotes its $j$-th component. Therefore, the vectors containing all inputs and all estimates in the network are $u(t), x(t) \in \reals^n$, respectively.

%%%%

\begin{assumption}[Differentiable Input Vector]
\label{ass:consensus.input.diff.almost.everywhere}
    The input vector $u(t)$ is differentiable almost everywhere.
\end{assumption}

%%%%

\begin{assumption}[Bounded Input Derivative]
\label{ass:consensus.bounded.input.derivative}
    The input vector $u(t)$ is absolutely continuous and there is a known upper-bound $L > 0$ to its time derivative
    %
    \begin{align}
    \label{eq:consensus.bounded.input.derivative}
        \max_{j\in\setlabels} \abs{\dot u_j(t)} \leq L \ ,\ \ \forall t
    \end{align}
    %
    which is defined almost everywhere.
\end{assumption}

%%%%

Depending on the context, the function $\max(.)$ can denote either the maximum component of a vector or the maximum element in a set. Also, recall the sign function definition
%
\begin{align}
    \sign(\zeta) = \left\{\begin{array}{lcl}
        -1 &,& \zeta < 0
        \\
        \phantom{-}1 &,& \zeta > 0
    \end{array}\right.
\end{align}
%
However, we consider that if $\zeta = 0$ is not a sliding-surface, $\sign(\zeta) = 0$ at $\zeta = 0$. Otherwise, $\sign(\zeta)$ is undefined at $\zeta = 0$. We consider the following linear approximation of the sign function,
%
\begin{align}
    \signeps(\zeta) = \left\{\begin{array}{lcl}
        \sign(\zeta) &,& \abs{\zeta} > \epsilon
        \\
        \zeta / \epsilon &,& \abs{\zeta} \leq \epsilon
    \end{array}\right.
\end{align}
%
for any positive scalar $\epsilon > 0$.

%%%%

Considering the above definitions, the purpose of this chapter is to develop an algorithm such that all estimates $x_j$ are driven toward the maximum input $\max(u)$ in the network. In other words, for $t > t^*$, $\abs{x_j(t) - \max(u(t))} < \epsilon$, for all $j \in \setlabels$ and for an arbitrarily small scalar $\epsilon > 0$, where the convergence time $t^* > 0$ can be made arbitrarily close to zero.


%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% | SECTION | %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
\section{Dynamic Consensus Estimator}
\label{chap:consensus.estimator}
In this section, we formulate the problem and present the sliding-mode based maximum consensus algorithm.

%%%%

Let $\graph$ be the graph of a network such that \cref{ass:consensus.graph} is satisfied. For this class of networks, consider the update rule
%
\begin{subequations}
\label{eq:consensus.control.law}
\begin{align}
    \label{eq:consensus.control.law.dyn}
    \tau \dot x_j &= (1 + \alpha) \, \signeps(\hat e_j) + \sign(e_j)
    \\
    \label{eq:consensus.control.law.error.estimate}
    \hat e_j &= \max\left(\setestimates_j\right) - x_j
    \\
    \label{eq:consensus.control.law.error.input}
    e_j &= \max\left(\setinputs_j\right) - x_j
\end{align}
\end{subequations}
%
where $\hat e_j(t) \in \reals$ is the error between the node estimate and the maximum estimate it knows, $e_j(t) \in \reals$ the error between the node estimate and the maximum input it knows, $\tau > 0$ is a scalar that controls the convergence rate, and $\alpha \in \left(0,1\right)$ and $\epsilon > 0$ are other design parameters.

%%%%

The first component of~\eqref{eq:consensus.control.law} is responsible for driving all estimates toward a common value, which is the highest in the entire network. The second component is responsible for driving the estimates toward the maximum input.

%%%%

We assume that $u(t)$ is differentiable almost everywhere, and that its rate of change $\dot u(t)$ is bounded, such that \cref{ass:consensus.input.diff.almost.everywhere,ass:consensus.bounded.input.derivative} are true. \Cref{ass:consensus.bounded.input.derivative} is important to enable the control designer to select an appropriate value for the parameter $\tau$ in~\eqref{eq:consensus.control.law.dyn}.

%%%%

\begin{lemma}[Convergence to $\max(x)$]
\label{lem:consensus.convergence.estimates}
    Let the graph $\graph$ satisfy \cref{ass:consensus.graph,ass:consensus.input.diff.almost.everywhere,ass:consensus.bounded.input.derivative}, and let all nodes update their estimates through the update rule~\eqref{eq:consensus.control.law}, starting at an initial time $t_0$. Then, with $\tau < \alpha/L$, the network achieves a consensus and all estimates converge to $\max(x) - x_j \leq \order{\epsilon}$ after a finite time $t_1 \leq t_0 + \order{\tau}$, remaining therein for $t \geq t_1$.
\end{lemma}
\begin{proof}
    At any given time, a node's estimate is lower than or equal to the highest estimate that it is aware of, i.e. $x_j(t) \leq \max(\setestimates_j(t))$, $\forall j \in \setlabels$. Thus, considering also the $\epsilon$-vicinity of $\max(\setestimates_j)$, there are three possibilities for any node $j$: $x_j \leq \max(\setestimates_j) - \epsilon$, $\max(\setestimates_j) - \epsilon < x_j < \max(\setestimates_j)$, and $x_j = \max(\setestimates_j)$.

    \noindent {\bf$\bm{ x_j \leq \max(\setestimates_j) - \epsilon }$\,.} In this case, it follows that $\hat e_j \geq \epsilon \iff \signeps(\hat e_j) = 1$, and~\eqref{eq:consensus.control.law.dyn} becomes
    %
    \begin{align}
        \tau \dot x_j &= 1 + \alpha + \sign(e_j) \geq \alpha
    \end{align}
    %
    with a solution
    %
    \begin{align}
    \label{eq:consensus.dyn.smaller.eps}
        x_j(t) &\geq x_j(t_0') + (\alpha/\tau)(t - t_0')
    \end{align}
    %
    where $t_0' > t_0$ is a time instant at which $x_j$ enters the region $x_j \leq \max(\setestimates_j) - \epsilon$. Therefore, while $x_j$ is not close to $\max(\setestimates_j)$ it increases at a rate $\alpha/\tau$.

    \noindent {\bf$\bm{ x_j = \max(\setestimates_j) }$\,.} In this case, $\hat e_j = 0 \iff \signeps(\hat e_j) = 0$, and~\eqref{eq:consensus.control.law.dyn} becomes
    %
    \begin{align}
        \tau \dot x_j = \sign(e_j)
    \end{align}
    %
    such that the dynamics of $e_j$ becomes
    %
    \begin{align}
        \dot e_j = \tau \frac{d}{dt} \max(\setinputs_j) - \sign(e_j)
    \end{align}
    %
    Therefore, as long as $\tau < 1/L$, $e_j = 0$ is a sliding-surface while $x_j = \max(\setestimates_j)$. Furthermore, to guarantee that the neighbors $i$ of node $j$ for which $\max(\setestimates_i) = x_j$ have estimates converging to $x_j$, one must have $\alpha/\tau > L \iff \tau < \alpha / L$. This conclusion follows from~\eqref{eq:consensus.dyn.smaller.eps}.

    These two cases are enough to show that $x_j \to \max(x)$, $\forall j \in \setlabels$, entering an $\epsilon$-vicinity of $\max(x)$ in finite time, since all estimates are either increasing, according to~\eqref{eq:consensus.dyn.smaller.eps} or bounded by their highest known input until another estimate exceeds it. When this happens, the exceeded estimate, for instance $x_i$, must track its current maximum known estimate $\max(\setestimates_i)$. This process repeats itself and propagates through the network until all estimates reach $x_j \geq \max(x) - \diamgraph\epsilon$, converging in a finite time
    %
    \begin{subequations}
    \label{eq:consensus.finite.time.bound.estimates}
    \begin{align}
    \label{eq:consensus.finite.time.convergence.estimates}
        t_1 &\leq t_0 +
            \Delta \left(\frac{\alpha}{\tau} - L\right)^{-1}
            \leq t_0 + \order{\tau}
        \\[0.5ex]
        \Delta &= \max\left(\begin{bmatrix}
            u(t_0) \\[0.5ex] x(t_0)
        \end{bmatrix}\right) - \min(x(t_0))
    \end{align}
    \end{subequations}
    %
    The propagation is guaranteed because $\graph$ is strongly connected. The factor $\diamgraph$ acts as a worst case bound because the estimate error might propagate through the network from $\max(x)$ to the node furthest from it. This distance is, at most, equal to the graph diameter $\diamgraph$.
\end{proof}

%%%%

From the perspective of consensus, \cref{lem:consensus.convergence.estimates} is enough to show that the network reaches a consensus with the proposed technique. It does not, however, establish any relationship between the nodes inputs and the consensus value. The next theorem shows that the update rule~\eqref{eq:consensus.control.law} is able to enforce tracking of the maximum network input.

%%%%

\begin{theorem}[Convergence to $\max(u)$]
\label{thm:consensus.convergence.inputs}
Let the graph $\graph$ satisfy \cref{ass:consensus.graph,ass:consensus.input.diff.almost.everywhere,ass:consensus.bounded.input.derivative}, and let all nodes update their estimates through the update rule~\eqref{eq:consensus.control.law}, starting at an initial time $t_0$. Then, with $\tau < \alpha/L$, the network achieves a consensus and all estimates converge to $\abs{\max(u) - x_j} < \order{\epsilon}$ after a finite time $t^* \leq t_0 + \order{\tau}$, remaining therein for $t \geq t^*$.
\begin{proof}
    From \cref{lem:consensus.convergence.estimates} we already know that $\max(\setestimates_j) - x_j \leq \epsilon$ and $\max(x) - x_j \leq \diamgraph\epsilon$ hold $\forall j \in \setlabels$ for $t \geq t_1$. Thus, it suffices to show that $\max(x) \to \max(u)$. The proof is then split into two parts. First, we show that if $\max(x) < \max(u)$, all estimates increase. Otherwise, if $\max(x) > \max(u)$, all estimates decrease.

    \noindent {\bf$\bm{ \max(x) < \max(u) }$\,.}
    Let $j : u_j = \max(u)$, which implies $u_j = \max(\setinputs_j)$, and let
    %
    \begin{align}
    \label{eq:consensus.proof.k.set}
        k \in \left\{
           i \in \setlabels : (j,i) \in \setedges
        \right\} \cup \{j\}
    \end{align}
    %
    that is, $k$ correspond to all nodes that node $j$ has access to, including itself. Considering these nodes and $t > t_1$, such that $\max(\setestimates_j) - x_j \leq \epsilon \iff \signeps(\hat e_j) = \hat e_j / \epsilon$,
    %
    \begin{align}
    \label{eq:consensus.proof.thm.dyn_xk}
        \tau \dot x_k = \frac{1 + \alpha}{\epsilon} \, \hat e_k + \sign(e_k)
    \end{align}

    Since $\max(x) < \max(u)$, then $\sign(e_k) = \sign(\max(u) - x_k) = 1$. Furthermore, from its definition, it follows that $\hat e_k \geq 0$, and equation~\eqref{eq:consensus.proof.thm.dyn_xk} can be converted to the inequality
    %
    \begin{align}
        \tau \dot x_k \geq 1
    \end{align}
    %
    which yields
    %
    \begin{align}
        x_k(t) \geq x_k(t_2) + (t - t_2) / \tau
    \end{align}
    %
    where $t_2 \geq t_1$ is any time instant for which $\max(x) < \max(u)$. Hence, for $t > t_1$, $\max(\setestimates_j)$ must reach $\max(u)$ in a finite time
    %
    \begin{subequations}
    \label{eq:consensus.proof.bound.time.a}
    \begin{align}
        t_1^* &\leq t_1 + \Delta^* \left(\frac{1}{\tau} - L\right)^{-1}
        \\[0.5ex]
        \Delta^* &= \abs{\max(u(t_1)) - \max(\setestimates_j(t_1))}
    \end{align}
    \end{subequations}
    %
    From \cref{lem:consensus.convergence.estimates}, $\abs{x_j -\max(u)} \leq \diamgraph\epsilon$, $\forall j \in \setlabels$, is reached in a finite time bounded by~\eqref{eq:consensus.proof.bound.time.a}.

    \noindent {\bf$\bm{ \max(x) > \max(u) }$\,.}
    Consider the error function
    %
    \begin{align}
        e = \max(x) - \max(u) = 0
    \end{align}
    %
    with time derivative
    %
    \begin{align}
        \tau \dot e = \sign(e_i) - \tau \frac{d}{dt} \max u(t)
    \end{align}
    %
    where $i : x_i = \max(x)$ and $\signeps(x_i) = 0$ was omitted because $x_i$ is not in sliding-mode. Since $\max(x) > \max(u)$, then $\sign(e_i) = -1$. Therefore, the following is valid whenever $\max(x) > \max(u)$:
    %
    \begin{subequations}
    \begin{align}
        \tau \dot e &\leq \tau L - 1
        \\
        e(t) &\leq e(t_3) - \left(\frac{1}{\tau} - L\right) (t-t_3)
    \end{align}
    \end{subequations}
    %
    where $t_3 \geq t_1$ is any time instant for which $\max(x) > \max(u)$. Hence, for $t > t_1$ and $\max(x) > \max(u)$, the maximum estimate $\max(x)$ reaches $\max(u)$ in a finite time
    %
    \begin{align}
    \label{eq:consensus.proof.bound.time.b}
        t_2^* &\leq t_1 + \Delta^* \left(\frac{1}{\tau} - L\right)^{-1}
    \end{align}
    %
    which equals the bound~\eqref{eq:consensus.proof.bound.time.a} for the previous case. Once again, invoking \cref{lem:consensus.convergence.estimates}, $\abs{x_j -\max(u)} \leq \diamgraph\epsilon$, $\forall j \in \setlabels$, is reached in a finite time bounded by~\eqref{eq:consensus.proof.bound.time.b}.

    Finally, since it was shown that $\max(x)$ is driven toward $\max(u)$, we conclude from~\eqref{eq:consensus.finite.time.bound.estimates}, \eqref{eq:consensus.proof.bound.time.a}, and~\eqref{eq:consensus.proof.bound.time.b} that $\abs{\max(u) - x_j} \leq \diamgraph\epsilon \leq \order{\epsilon}$ is reached in a finite time
    %
    \begin{align}
    \label{eq:consensus.finite.time.bound.inputs}
        t^* \leq t_0 + t_1 + \Delta^* \left(\frac{1}{\tau} - L\right)^{-1} \leq t_0 + \order{\tau}
    \end{align}
\end{proof}
\end{theorem}

\begin{remark}[Finite Time Upper-Bounds]
    It is worth pointing out that both convergence time upper-bounds~\eqref{eq:consensus.finite.time.bound.estimates} and~\eqref{eq:consensus.finite.time.bound.inputs} are very conservative, since, to compute them, it is assumed that the inputs are always growing at their maximum possible rate $L \geq \max_{j\in\setlabels} \abs{\dot u_j}$.
\end{remark}

\begin{remark}[Application to Minimum Consensus Estimation]
    The proposed consensus estimator can be easily applied to the problem of finding the minimum overall input in a network, instead of the maximum overall input. For that, one need only change the $\max(.)$ functions by $\min(.)$ functions, and no other modification is needed, such that the consensus dynamics is written as follows.
    %
    \begin{subequations}
    \label{eq:consensus.control.law.min}
    \begin{align}
        \label{eq:consensus.control.law.min.dyn}
        \tau \dot x_j &= (1 + \alpha) \, \signeps(\hat e_j) + \sign(e_j)
        \\
        \label{eq:consensus.control.law.min.error.estimate}
        \hat e_j &= \min\left(\setestimates_j\right) - x_j
        \\
        \label{eq:consensus.control.law.min.error.input}
        e_j &= \min\left(\setinputs_j\right) - x_j
    \end{align}
    \end{subequations}
\end{remark}

\Cref{thm:consensus.convergence.inputs} guarantees that all estimates track the maximum network input, with an arbitrarily small error of order $\order{\epsilon}$. Naturally, in practical discrete-time implementations, even though the theory ensures arbitrarily fast convergence rates and small tracking errors, there is a tradeoff between improving these values and selecting an appropriate sampling period. The faster the system dynamics, the smaller the sampling period. Likewise, the smaller the desired tracking error, the smaller the sampling period. To help implementing the proposed consensus algorithm~\eqref{eq:consensus.control.law}, we highlight some implementation guidelines.

\subsection{Discrete-Time Implementation Guidelines}
\label{chap:consensus.guidelines}
To avoid undesired chattering, we have experienced better results using the trapezoidal integration rule and using $\signeps(e_j)$ instead of $\sign(e_j)$ in~\eqref{eq:consensus.control.law.dyn}. For the initial states, we suggest using $x_j(t_0) = u_j(t_0)$. Let $p_\mathrm{error} > 0$ and $p_\mathrm{rate} > L$ denote the desired maximum error and minimum convergence rate, with $L$ from \cref{ass:consensus.bounded.input.derivative}. Using these specifications, the control parameters are defined as
%
\begin{subequations}
\label{eq:consensus.parameters.design}
\begin{align}
    \epsilon &= p_\mathrm{error} / \diamgraph
    \\
    \tau &= \alpha / p_\mathrm{rate}
\end{align}
\end{subequations}
%
Although we let $\alpha \in (0,1)$, we usually select $\alpha = 0.5$.

%%%%

There are two basic rules to select the sampling period. It can be either a function of the convergence rate or a function of the desired tracking error. To ensure convergence and that both specification are met, the sampling period $t_s$ should be
%
\begin{align}
\label{eq:consensus.sampling.design}
    t_s \leq \min\left(
        \epsilon\ ,\ \tau/100
    \right)
\end{align}
%
Naturally, if the sampling period $t_s$ is pre-defined, good choices of $\epsilon$ and $\tau$ are
%
\begin{subequations}
\begin{align}
    \epsilon &\geq t_s
    \\
    \tau &\geq 100 \, t_s
\end{align}
\end{subequations}
%
There is a margin on $\tau$, such that the $10^2$ factor can be relaxed to $10$ without much impact on performance.

%%%%

Finally, we stress that it is essential to select an appropriate sampling period. Otherwise, there might be convergence issues that may hinder the algorithm performance. If reducing the sampling period or increasing $\tau$ are not viable options, one might actually lower the parameter $\tau$. Although this seems counterintuitive, it mitigates the problem, at the expense of increasing chattering.

%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% | SECTION | %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
\subsection{Numerical Simulation Example}
\label{chap:consensus.numerical}
In this section, we illustrate the proposed consensus algorithm properties through two numerical simulations. The first one serves to illustrate the algorithm tracking performance and its convergence properties, while the second displays its robustness to the network size. The graphs topologies for these simulations are illustrated in \cref{fig:simulation.graphs}. Note that, regarding the network connectivity, the second topology is the worst possible, since every node has the same eccentricity, which equals the network diameter $\diamgraph[\graph_2] = 99$.

\begin{figure}[h]
    \centering
    \includegraphics[scale=1]{figs/tikz/simulation_graph}
    \caption{Topology of the two graphs, $\graph_a$ on the left and $\graph_b$ on the right, considered in the simulations. For simplicity, we draw only 13 nodes of $\graph_b$, but the actual simulation runs with 100 nodes.}
    \label{fig:simulation.graphs}
\end{figure}

As suggested in \cref{chap:consensus.guidelines}, all simulations are solved using the trapezoidal integration rule.

\subsubsection{Small Network}
The first simulation consists of a graph $\graph_a$ with four nodes connected as in \cref{fig:simulation.graphs}(a), each with a sinusoidal input
%
\begin{align}
    \label{eq:consensus.simulation.input.a}
    u_j(t) = \sin(2\pi t/j)
\end{align}
%
Nodes are numbered starting from the topmost node in \cref{fig:simulation.graphs}(a) and increase clockwise until the last node is reached. The target consensus value $\max(u)$ is shown in \cref{fig:simulation.consensus.target}. All parameters, together with those of the other simulation, are listed in \cref{tab:simulation.params}. The control parameters are chosen according to~\eqref{eq:consensus.parameters.design} and~\eqref{eq:consensus.sampling.design} to achieve a desired maximum consensus error $p_\mathrm{error} = 0.01$ and a desired minimum convergence rate $p_\mathrm{rate} = 2\pi$.

\begin{table}[h]
\centering
\caption{Parameters used to run the simulations on graphs $\graph_a$ and $\graph_b$ of \cref{fig:simulation.graphs}.}
\label{tab:simulation.params}
\begin{tabular}{l|c|c|c|c}
                                & $\tau$            & $\alpha$          & $\epsilon$          & $t_s$             \\\midrule
    Simulation of $\graph_a$ & $8 \cdot 10^{-2}$ & $5 \cdot 10^{-1}$ & $2.5 \cdot 10^{-3}$           & $10^{-4}$         \\
    Simulation of $\graph_b$ & $5 \cdot 10^{-2}$ & $5 \cdot 10^{-1}$ & $10^{-3}$ & $10^{-4}$
\end{tabular}
\end{table}

\subsubsection{Large Network with Sparse Connectivity}
This simulation consists of a graph $\graph_b$ with one hundred nodes connected as in \cref{fig:simulation.graphs}(b), labeled clockwise, with ramp inputs
%
\begin{subequations}
\label{eq:consensus.simulation.input.b}
\begin{align}
    u_j(t) &= \left(\frac{j}{n}\right) t + b_j
    \\
    b_{j-1} &= \left(\frac{j-1}{jn}\right) t_f + b_j
    \\
    b_n &= 1 - t_f \ \ ,\ \ t_f = 10
\end{align}
\end{subequations}
%
Out of curiosity, note that $\lim_{n \to \infty} \max(u(t)) = [(t/t_f)^2 - 1] \, t_f/2 + 1$, for $t \in [0,t_f]$, with $u(t)$ from~\eqref{eq:consensus.simulation.input.b}, is a parabola. The target consensus value $\max(u)$ is shown in \cref{fig:simulation.consensus.target}. All control and simulation parameters are listed in \cref{tab:simulation.params}. The control parameters are chosen according to~\eqref{eq:consensus.parameters.design} and~\eqref{eq:consensus.sampling.design} to achieve a desired maximum consensus error $p_\mathrm{error} = 0.1$ and a desired minimum convergence rate $p_\mathrm{rate} = 10$.


\subsubsection{Simulation Results}

The simulation results are shown in \cref{fig:simulation.results} for both scenarios. Note that, as expected, during both simulations all nodes estimates converge to the maximum network input and proceed to track this value afterwards. The bottom graphs display the tracking errors, which remain smaller than the prescribed values of $0.01$ and $0.1$.

\begin{figure}[h]
    \centering
    \includegraphics[scale=1]{figs/tikz/max_consensus}
    \caption{Maximum consensus of networks $\graph_a$ and $\graph_b$ shown in \cref{fig:simulation.graphs}, with inputs~\eqref{eq:consensus.simulation.input.a} and~\eqref{eq:consensus.simulation.input.b}. In blue the value of $\max(u)$ and in light gray the nodes inputs $u_j$. Only the inputs $u_{1}$, $u_{33}$, $u_{66}$, and $u_{100}$ are shown for $\graph_b$.}
    \label{fig:simulation.consensus.target}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=1]{figs/tikz/results_b}%
    \hfill%
    \includegraphics[scale=1]{figs/tikz/results_a}
    \caption{Simulation results for networks $\graph_a$ and $\graph_b$, with inputs~\eqref{eq:consensus.simulation.input.a} and~\eqref{eq:consensus.simulation.input.b}, using the proposed consensus algorithm~\eqref{eq:consensus.control.law}. At the top, the solution of $\graph_a$. At the bottom, the solution of $\graph_b$, where only 25 out of the 100 nodes are shown.}
    \label{fig:simulation.results}
\end{figure}

Analyzing the results from the first simulation (topmost plots in \cref{fig:simulation.results}), we observe some of the convergence properties of the proposed consensus algorithm. A lot can be said about the interaction between nodes 2 (in solid black), 3 (in dashed black), and 4 (in solid red). Focusing on a short frame at the begining of the simulation, one can study several convergence properties discussed in the proof of \cref{lem:consensus.convergence.estimates}, and also get a good feeling of the algorithm transient behavior.

When the simulation starts, node 4 knows no higher estimate than its own, and, hence, $x_4$ tracks its own input, since $\max(\setinputs_4) = u_4$. Meanwhile, not only does node 3 knows an estimate higher than its own, $\max(\setestimates_3) = x_2$, but also $\max(\setinputs_3) = u_2 > x_3$. Thus, node's 3 estimate increases at a rate $(2+\alpha)/\tau$. At approximately $t = 8.4$ ms $x_3$ surpasses $x_4$, and node 4 will then start increasing its estimate at a rate $\alpha/\tau$, since $\max(\setestimates_4) = u_4 < x_4$, and follow $x_3$. Shortly after, at approximately $t = 11$ ms, $x_3$ surpasses $\max(\setinputs_3) = x_2$, node's 3 highest known input, and $x_3$ will then start increasing at a rate $\alpha/\tau$, the same as $x_4$. Both estimates continue to grow until $x_3$ reaches the $\epsilon$-vicinity below $x_2$ at approximately $t = 157$ ms. From this moment on, the network reaches a consensus and all estimates track $\max(u)$.

The results of the second simulation illustrate a phenomenon that, although very unlikely, can occur on some occasions. It is the error propagation from one node to another across a long path in the network, in this case, across the entire network. In the current example, it happens because $\max(u)$ is always changing from $u_j$ to $u_{j+1}$ as time goes by. These consecutive changes imply that $x_{j-1}$ is always chasing $x_j$. Since all estimates are increasing at the same rate, without ever reaching one another, the final consensus error for each node becomes
%
$$
    \abs{\max(u) - x_j}_{t \geq 10}
        \approx \delta(j, 100) \, r \, \epsilon
        = (100-j) \, r \, \epsilon
$$
%
where $\delta(j,100)$ is the distance from node $j$ to node 100 and $r \in (0,1)$ a ratio which determines the separation between each estimate. On this simulation scenario, we have $r \approx 0.7$. On average, the overall final consensus errors is reduced if the node inputs of this second example are randomly reordered or, even better, if more edges are added to the network, such that the network diameter $\diamgraph[\graph_2]$ decreases.

%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% | SECTION | %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
\section{Privacy-Preserving Dynamic Consensus Estimator}
\label{chap:consensus.privacy}
As it is, the proposed consensus estimator fails to provide privacy with respect to the nodes inputs and estimates. From the update rule~\eqref{eq:consensus.control.law}, it is clear that any node in the network is aware of the inputs and estimates from all of its neighbors. In this section, we propose a modification to the proposed consensus estimator such that it is able to deliver privacy with respect to the nodes inputs. For that, we avoid transmitting the nodes inputs to their neighbors. Instead, we rely on the neighbor nodes telling to which direction should a node update its estimate.

%%%%

\begin{remark}
    In this section, we develop a privacy preserving consensus estimator. We do not, however, develop the stability nor the convergence proof, but leave them as a topic for future research.
\end{remark}

%%%%

Consider the following modification to the consensus dynamics~\eqref{eq:consensus.control.law},
%
\begin{subequations}
\label{eq:consensus.control.law.privacy}
\begin{align}
    \label{eq:consensus.control.law.privacy.dyn}
    \tau \dot x_j &= (1 + \alpha) \, \signeps(\hat e_j) + \sign(u_j - x_j)
    \\
    \label{eq:consensus.control.law.privacy.error.estimate}
    \hat e_j &= \max\left(\setestimates_j\right) - x_j
    \\
    \label{eq:consensus.control.law.privacy.error.estimates}
    \setestimates_j &= \left\{
        x \in \reals : x = x_i + N_i, \, (j,i) \in \setedges 
    \right\} \cup \{x_j\}
\end{align}
\end{subequations}
%
where $N_i$ is a zero-mean random variable that adds noise to the estimate advertised by node $i$, and $\setestimates_j$ is modified to reflect that node $j$ only knows the estimates of its neighbors plus some noise $N_i$.

%%%%

The idea behind the above update rule is to protect the information about the node input and to mask the information about the node estimate. The node input is protected since it is never transmitted to any neighbor, and the estimates are masked by adding noise before transmitting them.
By not transmitting the inputs, one downside is an increase in the convergence time. Furthermore, the noise added to the estimates cause a deviation that is proportional to the noise power.

%%%%

This privacy preserving algorithm can also be modified to serve as a minimum consensus estimator. For that, one need only swap the $\max(.)$ function by the $\min(.)$ function in~\eqref{eq:consensus.control.law.privacy.error.estimate}.

%%%%

We do not prove the convergence or stability of the proposed privacy-preserving dynamic consensus estimator. Nonetheless, it is not unexpected that the modified consensus algorithm retains the properties of the one from \cref{chap:consensus.estimator}, but for a small residual error. The residual error is caused by the noise added to the argument of the $\signeps(.)$ function in~\cref{eq:consensus.control.law.privacy.dyn}, and, thus, can be made arbitrarily small by admitting more disclosure of the nodes estimates.