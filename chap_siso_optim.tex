\chapter{Single-Objective Real-Time Optimization}
\label{chap:siso.esc}
In this chapter, the previous results on output-tracking via output-feedback are applied
to real-time optimization.
%
The optimization technique developed here is based on earlier results
by~\textcite{drakunov1995abs,oliveira2012global}, and may be placed under the category of
extremum-seeking control (ESC).

%%%%

Optimization problems arise in most engineering applications. Given a set of objective functions and system constraints, optimization is used to determine the optimal process input and design parameters that are needed to meet these conditions. Constraints are related to the system's optimal performance, but they can also drive the choice of an optimization algorithm. For instance, when modifications must be made in real-time, and the objective functions are measured but unknown, the problem becomes harder to solve since process information and computational power are limited. In the optimal control literature, extremum-seeking control (ESC) algorithms are a suitable choice when it comes to dealing with real-time optimization of systems with unknown (or partially unknown) models.

%%%%%

When the objective function is known, or at least its model is known, but the parameters are unknown, it is common to use other techniques from the optimal control literature, such as model predictive control. Optimal control is an exciting and vast branch of control theory, from which, unfortunately, we cover only the small subset related to ESC. For an in-depth view of optimal control, the reader is referred to \parencite{liberzon2011calculus}.

%%%%

In the following section, we describe the optimization problem and how it relates to the output-tracking of processes with unknown control direction. The objective of this section is to help the reader understand this connection while also writing the underlying assumptions needed to establish it.

%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% | SECTION | %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
\section{Problem Formulation}
\label{chap:siso.esc.formulation}
In this Thesis, we write single-objective optimization with one decision variable as a standard optimal control problem\footnote{We write optimization as a minimization problem, but all results are readily applied to maximization problems.}:
%
\begin{subequations}
\label{eq:siso.optim}
\begin{align}
    \label{eq:siso.optim:obj}
    \underset{u}{\mathrm{min}} : \,\, & \nu = J(y)
    \\
    \label{eq:siso.optim:dx}
    \mathrm{s.t.} : \,\, & \dot x(t) = f(x) + g(x)u(t)
    \\[0.5ex]
    \label{eq:siso.optim:y}
    & y(t) = h(x)
\end{align}
\end{subequations}
%
The function $J : \reals \mapsto \reals$ is called the objective function, and represents a performance that must be optimized, e.g. the power output in power plants, the temperature in heat exchangers, and a company's profit.
%
In~\eqref{eq:siso.optim}, the objective is written as a function the process output.
%
Several variations of this problem include the control effort $u(t)$ in the objective function, in order to waste less control energy, or an explicit dependence on time.
%
On the other hand, we only take the control effort into account in the process dynamics, which is seen as an equality constraint to the optimization problem.
%
It is usual to consider other equality and inequality constraints, but they are not accounted for in this work.

%%%%

To consider the above optimization problem solved, one must find the control input $u(t)$ that drives the process output such that the objective function $J(y)$ is minimized over time. The two usual approaches to this problem are (i) to solve it offline and then apply the pre-computed control effort over a fixed period of time, or (ii) to continuously update the control input. A widely adopted technique that can tackle both of the approaches above simultaneously is model predictive control \parencite{camacho2007model}. Extremum-seeking falls into the category of continuously updating the control input.

%%%%

If we take the objective function and differentiate it with respect to time,
%
\begin{align}
    \dot\nu(t) = \frac{\partial J(y)}{\partial y} \left[
        L_f h(x) + L_g h(x) u(t)
    \right]
\end{align}
%
the connection to output-tracking of systems with unknown control direction becomes apparent.
%
With respect to the process output $y$, the high-frequency gain is $L_g h(x)$.
%
However, if we take the objective as the process output, the high-frequency gain becomes
%
\begin{align}
\label{eq:siso.esc.hfg}
k_p(x) = \frac{\partial J(y)}{\partial y}L_g h(x)
\end{align}
%
which is only a function of $x$, since the mapping from $y$ to $x$ is static. Thus, even if the process HFG does not change signal, the HFG with respect to the objective changes every time the output crosses the optimal value. For example, when dealing with minimization, $\partial J/\partial y < 0$ to the left of the minimum and $\partial J / \partial y > 0$ to the right.

%%%%

\begin{remark}
For simplicity, in what follows we ignore the variable $\nu$ and use only $y = h(x)$ as the objective function. This simplification does not impose any loss of generality, since we might consider $\nu = J \circ h(x) = h'(x)$. We use the notation $y = h(x)$ to keep the results closer to the ones already established for output-tracking.
\end{remark}

%%%%

To adapt the previous output-tracking results from extremum-seeking, some changes are in order since \cref{ass:hfg.bounded} is no longer valid. Also, to derive global results on ESC, some assumptions on the output function are needed. Otherwise, convergence to local optima can still be established, as demonstrated at the very end of \cref{chap:siso.esc.affine}. These assumptions are provided below.

%%%%

\begin{assumption}[Unique Extremum]
\label{ass:smooth.unique.maximizer}
The output function $h(.)$ in~\eqref{eq:smc.model.siso.out} is continuous and
differentiable almost everywhere in its domain and have a unique strict global
minimizer $x^*$, with minimum $y^* = h(x^*)$.
\end{assumption}

%%%%

\begin{assumption}[Known Gradient Bound]
\label{ass:bounded.output.deriv}
For any chosen $\Delta > 0$, there exists $\bar \Delta(\Delta) > 0$ and known
constants $L(\Delta)$ and $\underbar k_p(L)$ such that
%
\begin{align}
\begin{split}
    &L \leq \norm{ \frac{\partial h(x)}{\partial x} }
        \quad \mathrm{and} \quad
        0 < \underbar k_p(L) \leq \abs{k_p(x)} = \abs{L_g h(x)}
    \\
    &\forall x \not\in D_\Delta \hspace{-0.15em} = \hspace{-0.15em}
    \left\{
        x \in \reals^n : \norm{x - x^*} \leq \bar\Delta
        \, , \,
        \abs{y(x) - y^*} \leq \Delta
    \right\}
\end{split}
\end{align}
%
holds uniformly on $t$.
%
This means that a lower bound $L$ for the derivative can be established for any
$\Delta$-neighborhood of the optimum $y^*$ and that the system remains
controllable outside this vicinity.
\end{assumption}

Note that \cref{ass:bounded.output.deriv} replaces \cref{ass:hfg.bounded} close to the optimizer and is equivalent to the latter when the states are outside of the $\Delta$-neighborhood, i.e. $\forall x \not\in D_\Delta$.

%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% | SECTION | %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
\section{Continuous Sliding-Mode Extremum-Seeking-Controller for Input-Affine Processes}
\label{chap:siso.esc.affine}
To implement the extremum-seeking solution, the control law developed in
\cref{chap:siso.affine} is used to track a monotonic function.
%
Since the solution presented here is for minimization problems, the output
reference $y_m : \preals \mapsto \reals$ is monotonically decreasing, converges
to a constant value, i.e. $\exists$ $\lim_{t \to \infty} y_m(t)$, and chosen
such that there exists $t^* \geq 0$ for which $t \geq t^* \implies y_m(t) \leq y^*$.
%
This last requirement ensures that the model reaches the optimum, and it is
reasonable since bounds for the optimal value are usually known in practice.

\begin{remark}
To adapt this control law for maximization problems, one need only change $y_m(.)$
from a monotonically decreasing to a monotonically increasing function, such that
for $t \geq t^* \implies y_m(t) \geq y^*$.
\end{remark}

%%%%

Below, we enunciate and prove the theorem that states the results of the sliding-mode controller for real-time optimization. Considering our general formulation with a sigmoid function and the function $f_e(e)$ in the $\sigma$-dynamics, our previous choice of Lyapunov function~\eqref{eq:smc.siso.proof.lyapunov} based on the absolute value makes the proof of the next theorem easier.

%%%%

\begin{theorem}
\label{thm:siso.esc}
Consider system~\eqref{eq:smc.model.siso} with output error~\eqref{eq:error.tracking.siso}
based on a differentiable and monotonically decreasing output model $y_m(t)$, and control law~%
\eqref{eq:sigma.tracking.siso}, \eqref{eq:control.siso},
%
\begin{align}
    f_e(e) = \lambda\sigmoid{}_{\bar\epsilon}(e)
\end{align}
%
with $\lambda > 0$ and $\bar\epsilon > 0$.
%
Then, the extremum-seeking error
%
\begin{align}
\label{eq:error.esc.siso}
e^*(t) = y(t) - y^*
\end{align}
is uniformly globally practical
asymptotically stable, with ultimate bound $\abs{e^*} \leq \Delta + \big( 1 +
\lambda/\delta \big) T \leq \Delta + \order{T}$.
%
\end{theorem}
%
\begin{proof}
To prove that oscillations above the minimum $y^*$ are bounded by
$\abs{y - y^*} \leq \Delta + \order{T}$, it is necessary and sufficient to show
that after regaining controllability, i.e. $x \not\in D_\Delta \iff \abs{y -
y^*} > \Delta$, at a time $t_0$, the sliding variable $\sigma$ will enter real sliding-%
mode at $t_1$, such that $\abs{y(t_0) - y(t_1)} \leq \order{T}$.
%
Thereafter, the error decreases until $\abs{y - y^*} \leq \Delta$ and the system
loses controllability once again.
%
Hence, $y(t_1)$ is the maximum value the output is allowed to reach before
decreasing, and it is already a worst case estimate.

%%%%

This proof is made in two steps.
%
First, it is shown that the amount of time needed for $\sigma$ to enter real
sliding-mode is $t_1 - t_0 \leq \order{T}$.
%
Second, this implies that the output distances itself from the
$\Delta$-neighborhood by $\abs{y(t_1) - y(t_0)} \leq \order{T}$.

%%%%

At time $t_0$, when the system leaves $D_\Delta$, controllability is
regained and, assuming that $\sigma$ is not in real sliding-mode, the Lyapunov
function~\eqref{eq:smc.siso.proof.lyapunov} derivative satisfies~%
\eqref{eq:smc.siso.proof.lyapunov.deriv}.
%
Therefore,
%
\begin{align*}
&V(t_1) - V(t_0) = \int_{t_0}^{t_1} \dot{V}(\tau) d\tau \leq -\delta (t_1 - t_0)
\\
&t_1 - t_0 \leq \left[ V(t_0) - V(t_1) \right] / \delta
\end{align*}
%
from the proof of \cref{thm:siso.tracking}, we know that $V(t_0) - V(t_1) \leq T$, therefore
%
\begin{align}\label{eq:esc.siso.proof.tb-ta}
    &t_1 - t_0 \leq T / \delta \leq \order{T}
\end{align}

Recall the sliding variable $\sigma$ definition \eqref{eq:sigma.tracking.siso}.
%
From this equation and the time difference inequality \eqref{eq:esc.siso.proof.tb-ta},
%
\begin{align}
\nonumber
&\sigma(t_1) - \sigma(t_0) = e(t_1) - e(t_0) +
    \int_{t_0}^{t_1} f_e(e) d\tau
\\
\nonumber
&\abs{e(t_1) - e(t_0)} \leq
    \abs{\sigma(t_1) - \sigma(t_0)} +
    \int_{t_0}^{t_1} \abs{f_e(e)} d\tau
\\
\nonumber
&\abs{e(t_1) - e(t_0)} \leq
    \abs{\sigma(t_1) - \sigma(t_0)} +
    \lambda \int_{t_0}^{t_1}d\tau
\\
&\abs{e(t_1) - e(t_0)}
    \leq \left( 1 + \lambda/\delta \right) T
    \leq \order{T}
\end{align}
%
where $\abs{\sigma(t_1) - \sigma(t_0)} \leq T$ was used.
%
From the output error definition~\eqref{eq:error.tracking.siso},
%
\begin{subequations}
\begin{align}
\label{eq:esc.siso.proof.delta.y.a}
\abs{y(t_1) - y(t_0)} & \leq \abs{e(t_1) - e(t_0)}
    + \abs{y_m(t_1) - y_m(t_0)}
\end{align}
\end{subequations}
%
Since $y_m(t)$ maps to a complete set (the real line) and converges to a constant value,
it forms a Cauchy sequence, implying $\abs{y_m(t_1) - y_m(t_0)} \to 0$ as $t \to \infty$.
%
Thus, ultimately,
%
\begin{align}
    \abs{y(t_1) - y(t_0)} & \leq \abs{e(t_1) - e(t_0)}
        \leq \left( 1 + \lambda/\delta \right) T
        \leq \order{T}
\end{align}
%
Since $y(t_1) - y^* = y(t_1) - y(t_0) + y(t_0) - y^* = y(t_1) - y(t_0)
+ \Delta$, it follows that
%
\begin{align}
    \abs{y(t_1) - y^*} \leq (1 + \lambda/\delta)T + \Delta
        \leq \order{T} + \Delta
\end{align}
%
which concludes the proof.
\end{proof}

%%%%

\begin{remark}
If for some time $t_2 \in (t_0, t_1)$ the system state happens to enter
$D_\Delta$ such that $\abs{y-y^*} \leq \Delta$, the distance $\abs{y(t_2) -
y(t_0)} \leq \abs{y(t_1) - y(t_0)}$, since it is assumed that the output
distances itself from $y(t_0)$ during the interval $[t_0, t_1]$.
%
Therefore, the bound $\abs{y - y^*} \leq \Delta + \order{T}$ remains valid.
\end{remark}

%%%%

If there are multiple isolated optima, convergence to one of these
local optima can still be established.
%
For that, the bound $L$ computed for the $\Delta$-vicinity in
\cref{ass:bounded.output.deriv} must hold in a neighborhood
%
\begin{align}
    D_\Delta \hspace{-0.15em} = \hspace{-0.15em}
        \left\{
            x \in \reals^n : \norm{x - x^*} \leq \bar\Delta
            \, , \,
            \abs{y - y^*} \leq \Delta + (1 + \lambda/\delta)T
        \right\}
\end{align}
%
This guarantees that the output will remain bounded to the $\Delta$-vicinity of one of the optima and will not exit this vicinity.

%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% | SECTION | %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
\section{Stabilization of Systems with Unknown Output Sign}
\label{chap:siso.output.sign}
Constraints on a mobile robot sensor configuration or sensor malfunction
are some of the reasons for uncertain output measurements.
%
For instance, imagine a mobile robot carrying a single range-finder and placed
at a known position, but unknown orientation, from some wall.
%
The control objective is to make the robot face the wall.
%
This scenario is depicted in \cref{fig:mobile.robot}.
%
Note that, if the distance from the wall is given $l_w > 0$, and the sensor
measures a distance $l_s > 0$, the vehicle measured orientation with respect to
the wall, denoted $\theta_s$, is
%
\begin{align}
    \theta_s = \mathrm{arccos}(l_w / l_s) = \abs{\theta}
\end{align}
%
where $\theta$ is the actual orientation.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\linewidth]{figs/tikz/mobile_robot_wall}
    \caption{Illustration of a mobile robot carrying a laser range-finder and
    measuring its distance from a wall.}
    \label{fig:mobile.robot}
\end{figure}

This example motivates the development of a control law for cases where only
the output absolute value is available for measurement.
%
Thus, consider the dynamics
%
\begin{subequations}
\label{eq:smc.model.outsign}
\begin{align}
    \dot x(t) &= f(x) + g(x)u(t)
    \\
    y(t)      &= h(x)
    \\
    y_s(t)    &= \abs{y(t)}
\end{align}
\end{subequations}
%
with state vector $x : \prealsE \mapsto \reals^n$, unmeasured output $y : \prealsE
\mapsto \reals$ and measurement $y_s(t) : \prealsE \mapsto \preals$.
%
Let $L_g h(x)$ be bounded away from zero such that \cref{ass:hfg.bounded} holds,
and define the high-frequency gain with respect to the measured output
%
\begin{align}
    k_p(x) = \sign(y) L_g h(x)
\end{align}
%
The control objective is to asymptotically stabilize $y(t)$ at $y = 0$ using only
the measurable signal $y_s(t)$.
%
On a side note, we could also consider $L_g h(x)$ changing sign over time, but
then the problem would fall back to the same case as the one considered in
\cref{chap:siso.esc.affine}.

%%%%

Since $y_s = 0$ if and only if $y = 0$, and the absolute value function is continuous,
it follows that asymptotic stabilization of $y_s = 0$ implies asymptotic stabilization
of $y = 0$.
%
Hence, a straightforward approach is to treat the control problem as an extremum-seeking
problem.
%
The major differences from this formulation to other extremum-seeking schemes mentioned
in this work are that (i) the minimum output value is known ($y^* = 0$) a priori and (ii)
the HFG $k_p(x)$ is discontinuous at $y = 0$, where it changes sign.
%
These imply the following lemma, which replaces \cref{ass:bounded.output.deriv}.

%%%%

\begin{lemma}
\label{ass:bounded.output.deriv.stabilization}
There exists a known constant $L$ such that
%
\begin{align}
    L \leq \abs{ \frac{\partial h(x)}{\partial x} },\ %
        \forall x \in D_\Delta \hspace{-0.15em} = \hspace{-0.15em}
        \left\{x \in \reals^n : \abs{y} > 0\right\}
\end{align}
%
holds uniformly on $t$.
%
Therefore, $\Delta$ in \cref{ass:bounded.output.deriv} can be set to zero.
\end{lemma}
%
With the above lemma and the already mentioned assumptions, the following
theorem can be established.

%%%%

\begin{theorem}
\label{thm:siso.esc.stabilization}
Consider system~\eqref{eq:smc.model.outsign} with output error $e(t) = y_s(t)$
and control law~\eqref{eq:sigma.tracking.siso}, \eqref{eq:control.siso},
%
\begin{align}
    f_e(e) = \lambda\sigmoid{}_{\!\epsilon}\!(e)
\end{align}
%
with $\lambda > 0$.
%
Then, the output origin $y = 0$ is UGPAS, with ultimate bound
$\abs{e} \leq \big( 1 + \lambda/\delta \big) T \leq \order{T}$.
\end{theorem}
%
\begin{proof}
The proof is straightforward by considering the proof of \cref{thm:siso.esc}
and letting $y(t_0) = y^*$, where $t_0$ is the time at which controllability is lost.
\end{proof}

%%%%

\begin{remark}
Since controllability is only lost at $y(t_0) = y^*$ and immediately regained afterwards,
different from the ESC results presented in \cref{chap:siso.esc}, the error ultimate bound
does not include a parcel $\Delta$.
%
This is the same bound experienced by a tracking controller if one lets the HGF to change its
sign, without crossing $k_p(x) = 0$, as illustrated in the Van der Pol oscillator example of \cref{chap:siso.vanderpol}.
\end{remark}

%%%%%%%% +------------+ %%%%%%%%
%%%%%%%% | SUBSECTION | %%%%%%%%
%%%%%%%% +------------+ %%%%%%%%
\subsection{Application to Mobile Robots}
\label{chap:siso.mobile.robot}
The results presented in this section are quite general and apply to a wide class of
input-affine nonlinear plants with (almost) arbitrary output functions.
%
A subclass of such plants that also plays an important role in control theory is given below
%
\begin{subequations}
\label{eq:smc.model.integrator.stabilization}
\begin{align}
    \label{eq:smc.model.integrator.stabilization.x}
    \dot x(t) &= b(x) \left[u(t) + d(t)\right]
    \\
    \label{eq:smc.model.integrator.stabilization.y}
    y(t) &= \abs{x}
\end{align}
\end{subequations}
%
with state $x : \prealsE \mapsto \reals$, output $y : \prealsE \mapsto \reals$
and control input $u : \prealsE \mapsto \reals$.
%
An example system that follows~\eqref{eq:smc.model.integrator.stabilization} is the
orientation dynamics of differential drive mobile robots, as depicted in \cref{fig:mobile.robot}.

%%%%

For simplicity, we assume that $d(t)$ is not estimated and that the following bounds are
known
%
\begin{subequations}
\begin{align}
    \underbar{b}(y) \leq \abs{b(x)}
    \\
    d(t) \leq \bar d(t)
\end{align}
\end{subequations}
%
Using these bounds, the modulation function $\rho(t)$ is defined as
%
\begin{align}
    \rho(t, y) = \frac{\kappa}{\underbar{b}(y)}(\lambda + \delta) + \bar d(t)
\end{align}

For simulation purposes, let $b(x) = 1$ deg/s for $x \in [0,\pi/2] \cup [\pi,3\pi/2]$,
$b(x) = 0.4$ deg/s for $x \in (\pi/2,\pi) \cup (3\pi/2,2\pi)$, $d(t) = 0$ for $t \in
[0,2] \cup [4,\infty)$ and $d(t) = 10$ for $t \in (2,4)$.
%
The change in $b(x)$ represent a loss of traction due to slippage on the mobile robot wheels
as illustrated in \cref{fig:mobile.robot.slippage}.

%%%%

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.5]{figs/tikz/mobile_robot_slip}
    \caption{Representation of different slip coefficient impacting controllability.}
    \label{fig:mobile.robot.slippage}
\end{figure}

%%%%

As a worst case scenario, we take $\underbar b = 0.3$ deg/s and $\bar d(t) = 15$.
%
To recover from a 10 deg error in 1 second, let $\lambda = 10$ deg/s and in order to achieve
practical stability with at most $2$ deg error, let $\delta = 5$ deg/s and $T = 2/3$ deg.
%
The controller is implemented via saturated gain sigmoid functions, with
$\epsilon = 0.5$ deg, $\bar\epsilon = 1$ deg and $\rho(t,y) = 65$.
%
Finally, to meet the $2$ deg error specification, $(1 + \lambda/\delta)T \leq 2$, which,
for $\lambda = 10$ and $\delta = 5$, implies $T = 2/3$ deg.

%%%%

The simulation output is shown in \cref{fig:mobile.robot.sim}, where the controller
is implemented in discrete-time with sampling period of $10$ ms and the first-order
motor dynamics
%
\begin{align}
    \label{eq:sim.siso.motor.dynamics}
    H(s) = \frac{1}{0.015s + 1}
\end{align}
%
is considered. The sampling period is selected to be $10$ ms to bring the simulation closer to typical processing constraints of controllers used in mobile robotics.
%
The results are compared with a traditional $\sigmoid{}_{\epsilon}(.) = \sign(.)$ controller to
show that the continuous approximation via saturated gain
improves performance and reduces chattering.

%%%%

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{figs/sim/sim_mobile}
    \caption{Simulation results for the mobile robot example~%
    \eqref{eq:smc.model.integrator.stabilization} with motor dynamics~%
    \eqref{eq:sim.siso.motor.dynamics}.}
    \label{fig:mobile.robot.sim}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% | SECTION | %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
\section{Extension to Arbitrary Relative Degree}
\label{chap:siso.esc.arbitrary}
To conclude this chapter, we provide one final result that shows that the proposed extremum-seeking control law, the same as the output-tracking control law for systems with unknown control direction, can be used for systems with arbitrary relative degree. This is a short section, since it is a direct consequence of the results already discussed in \cref{chap:siso.arbitrary}. Since the results also follow from the direct application of Tikhonov's theorem under the same assumptions as in \cref{chap:siso.arbitrary}, we give the following theorem without proof.

%%%%

\begin{theorem}
    \label{thm:siso.esc.arbitrary}
    Consider system~\eqref{eq:smc.model.siso.fast} with slow model~\eqref{eq:smc.model.slow}.
    %
    Let the control law~\eqref{eq:control.siso} be designed such that \cref{thm:siso.esc}
    is satisfied for the slow model, for some objective function satisfying \cref{ass:smooth.unique.maximizer,ass:bounded.output.deriv}.
    %
    Then, the extremum-seeking error $e^*(t) = y(t) - y^*$ origin is PAS, with ultimate bound
    $\abs{e^*} \leq \Delta + \big( 1 + \lambda/\delta \big) T + \order{\mu} \leq \Delta
    + \order{T} + \order{\mu}$.
\end{theorem}

%%%%

An example that illustrates this extension to an arbitrary relative degree is the previous one for mobile robots, with results shown in \cref{fig:siso.fast.compare.sim}. In this example, the relative degree is equal to two, and given that the motor dynamics is faster than the ESC dynamics, stabilization (minimization of the output absolute value) is still possible given an appropriate choice of control parameters.

%%%%

Finally, our approach to arbitrary relative degree follows the usual generalizations made in the model-based and perturbation-based extremum-seeking literature. Nonetheless, one important advantage of our results, which are based on sliding-mode control, is that they are do not rely on any time-scale separation when the process has unit relative degree.