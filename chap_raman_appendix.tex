\chapter{Raman Amplifier Power Dynamics and Numerical Coefficients}\label{ap:pde}
Consider a simpler average field power model \parencite{B:04} which suffices for Raman amplifier fiber spans. This model is described by $N$ first-order nonlinear partial differential equations (transport PDEs):
%
$\tau_i \frac{\partial P_i}{\partial t}+\mu_i \frac{\partial P_i}{\partial z}=-\alpha_i P_i+ \sum_{j=1 \ (j \neq i)}^{N} c_{ij}P_i P_j$,
%
where $P_i(t,z)$ is the power (in W) of a propagating signal (data or pump)  at time $t$  and distance $z$ from the upstream end of the span (measured along the fiber), corresponding to the wavelength $\lambda_{i}$ (data or pump). The constants
$\tau_i$, $\mu_i$ and $\alpha_i$ denote, respectively, the propagation delays per unit length ($\mu
s/\mathrm{km}$), propagation directions (dimensionless), losses per unit length ($1/$km). The constant $c_{ij}$ denotes the coupling coefficient per unit length ($1/$W\,km) from the signal with wavelength $\lambda_j$ to the signal with wavelength $\lambda_i$ propagating in the fiber. This quantities $\tau_i(\lambda_i)$, $\alpha_i(\lambda_i)$ and $c_{ij}(\lambda_i,\lambda_j)$ are functions of the wavelengths propagating in the fiber. In this Thesis, we consider only counter-propagating pump signals, the data signals are co-propagating by default. The propagation direction $\mu_i$ ($i=n_p+1,\ldots,N$) is equal to $-1$ for pump signals, and it is equal to $+1$ for data signals.

For a compact form for the power dynamics, consider the following uncertain matrices: $\tau\!:=\!\diag\left(
\left[\begin{array}{ccc}\tau_1 & \ldots & \tau_N
\end{array}\right]\right)$, $\mu\!:=\!\diag\left(
\left[\begin{array}{ccc}\mu_1 & \ldots & \mu_N
\end{array}\right]\right)$, $A\!:=\!\diag\left(
\left[\begin{array}{ccc}\alpha_1 & \ldots & \alpha_N
\end{array}\right]\right)$ and $C \in \reals^{N \times N}$, where its
$ij$th entry is given by $c_{ij}$. The power dynamics can be rewritten as \parencite{dower2008extremum}:
%
\begin{align}\label{eq:dinamicanotempo}
\tau\frac{\partial P}{\partial t}+\mu \frac{\partial P}{\partial z}=-AP+\diag(P)C P \nonumber
\end{align}
%
with $P = \transp{\left[\begin{array}{ccc}P_1 & \ldots & P_N \end{array}\right]}$ partitioned in the form
%
%\begin{align}\label{cocozao}
$P=P(t,z):=\left[\begin{array}{c}
P_p(t,z)\\P_s(t,z)\end{array}\right] \in \reals^{N}$.
%\end{align}
%
These PDEs require the specification of one spatial and one temporal boundary condition per entry in $P$. The temporal initial condition is specified by an initial spatial power distribution $P(0,z)$. The spatial boundary conditions are specified by the vector of backward propagating pump powers and the vector of forward propagating data signal powers \parencite{KS:08,K:09}.

For the case where $4$ data signals and $2$ pump signals are considered, the physical parameters $\tau$, $A$, and $C$ were experimentally
characterized by the company {\em OFS Fitel Denmark Ap.}, consider the following wavelengths (em $\mbox{nm}$)
presented in the fiber:
$\Lambda_p \!=\! \transp{\left[\begin{array}{cc}
\!1442\!&\!
1480\!
\end{array}\right]}$ e $\Lambda_s \!=\! \transp{\left[\begin{array}{cccc}
\!1530\!&\!
1550\!&\!
1570\!&\!
1590\!
\end{array}\right]}$.
%
%.
%
The physical parameters are:
%
$10^6 \tau\!\!=\!\!\diag\left(\left[\begin{array}{cccccc}\!4.876\!\!&\!\!4.877\!\!&\!\!4.877\!\!&\!\!4.878\!\!&\!\!4.878\!\!&\!\!4.879\!\!\end{array}\right]\right)$,
%
$A\!\!=\!\!\diag\left(\left[\begin{array}{cccccc}\!0.058\!\!&\!\!0.051\!\!&\!\!0.047\!\!&\!\!0.045\!\!&\!\!0.045\!\!&\!\!0.045\!\!\end{array}\right]\right)$,
%
%
%
$$
C = \left[\!\begin{array}{cccccc}
0\!&\! -0.23 \!&\!-0.59\!&\!-0.62 \!&\!-0.18\!&\!-0.10\\
0.22\!&\!0\!&\!-0.17\!&\!-0.25\!&\!-0.39\!&\!-0.57\\
0.55\!&\!0.16\!&\! 0 \!&\!-0.12\!&\!-0.15\!&\!-0.21\\
0.58\!&\!0.24\!&\!0.12\!&\!0\!&\!-0.11\!&\!-0.14\\
0.17\!&\!0.37\!&\!0.15\!&\!0.11\!&\!0\!&\!-0.11\\
0.09\!&\!0.53\!&\!0.20 \!&\!0.14\!&\!0.11\!&\!0\\
\end{array}\!\right]
$$
%
and $\mu=\diag\left( \left[\begin{array}{cccccc}-1 \!&\! -1\!&\! 1 \!&\! 1\!&\! 1 \!&\!1\end{array}\right]\right)$.

For the case of $32$ data signals, we have chosen $16$ wavelengths equally spaced by 100 GHz from $1530$ nm to $1541.8$ nm for the first group and $16$ other wavelengths equally spaced from $1570$ nm to $1582.4$ nm for the second group. We have also changed the second pump wavelength to 1490 nm. All the coefficient matrices were approximated via interpolation of experimental data, as in  \parencite{dower2008extremum}. In particular, the entries of the matrices $\tau$, $A$ and $C$ were determined numerically as follows:
%
$$ \tau_{i}  = f_{\tau}(\lambda_i)\,, \quad A_{i}  = f_a(\lambda_i)\,, \quad C_{ij}  = f_c(\lambda_i,\lambda_j)$$
%
%%
where
%
$$f_{\tau}(\lambda) =  \sum_{i = 1}^{4}{ \hat{\eta}_i \lambda^i }\,, \quad f_a(\lambda) =  \sum_{i = 1}^{4}{\hat{\alpha}_i \lambda^{i-1}}$$
with
$$
\hat{\eta} =
\left[\begin{array}{ccccc}
     2.48 \times 10^{-4} &
    -1.03  &
     0.022 &
     0.022
\end{array}\right] \times 10^{-14}
$$
%
and
%
$$
\hat{\alpha} =
\left[\begin{array}{cccc}
     4.9  \times 10^{-9} &
    -1.9  \times 10^{-5} &
     2.33 \times 10^{-2} &
    -8.5
\end{array}\right]
$$
%
obtained via least square to fit the case of $4$ data signals.
%
By using the available Raman gain efficiency, experimentally characterized by the company {\em OFS Fittel Denmark Ap.}, as a function of the frequency difference between the signal and the pump with wavelength of $1442$ nm, it is possible to determine the coefficient of the matrix $C$ via interpolation by using
%
\begin{align}
 f_c(\lambda, \eta) = \gamma(\lambda, \eta) \, \Gamma (\mathrm{sgn}(\lambda - \eta) \rho(\lambda, \eta))
\end{align}
%
where
%
%
\begin{align}\nonumber
\gamma(\lambda, \eta) &= \left \{ \begin{array}{ccc}
	\dfrac{1}{4} &,& \text{if} ~\lambda > \eta \\[1ex]
	0 &,& \text{if}~ \lambda = \eta \\[1ex]
	\dfrac{-\eta}{4 \lambda} &,& \text{if}~ \lambda < \eta
\end{array} \right.
\end{align}
%
$\Gamma(\zeta) = \sum_{i = 1}^{3} \hat{\kappa}_i \exp \left( - \hat{\beta} (\zeta - \hat{\zeta }_i )^2\right)$, with
%
$$
\hat{\kappa} = \left[\begin{array}{ccc}
     1.813 \times 10^{-1} &
     5.652 \times 10^{-1} &
    -3.117 \times 10^{-2}
\end{array}\right]
$$
%
and
%
$$
\hat{\zeta} = \left[\begin{array}{ccc}
    4.250 &
    1.282 \times 10^{1} &
    4.176 \times 10^{-1}
\end{array}\right]
$$
%
$\rho(\lambda, \eta) = 2.9979 \times 10^{5} \left( \frac{1}{\eta} - \frac{1}{\lambda}\right)$ and  $\hat{\beta}\,=5.1509 \times 10^{-2}$.
This choice of the interpolates came from \parencite{dower2008extremum}, which is commonly used in optical fibers.