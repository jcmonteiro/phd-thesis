\chapter{Conclusion}
In this Thesis, the main topic of study has been real-time optimization of single-objective and multi-objective problems, with single or multiple decision variables, and subject to the dynamics of an underlying process. Other contributions are made on global output tracking and dynamic consensus estimation. Although we start the thesis by describing the results on output-tracking, we follow a different path in this conclusion to highlight what we believe are the most relevant contributions.

Two distinct extremum-seeking sliding-mode controllers using only input-output information and periodic switching functions were introduced to solve multi-objective problems from two different perspectives. First, in \cref{chap:mimo.static}, we develop an algorithm capable of driving the trajectories of a process towards an equilibrium that corresponds to the Nash equilibrium of a multi-objective output map. Changing the way we compute the periodic switching function amplitude, in \cref{chap:mimo.affine}, we show that the same algorithm is also able to optimize multi-objective problems subject to the dynamics of a nonlinear input-affine process. Second, in \cref{chap:miso}, we develop an algorithm capable of driving the system trajectories toward an equilibrium that corresponds to the optimizer of a single-objective optimization problem with multiple decision variables. Assuming convex objective functions and taking the convex combination of the multiple objectives, a common technique for scalarization, we show that the solution is Pareto efficient. Our results extend the current theory of sliding-mode extremum-seeking control by significantly widening the class of solvable optimization problems to include those with nonlinear input-affine underlying processes, without relying on any time-scale separation nor producing undesired chattering.

A novel dynamic consensus estimator for maximum-value and minimum-value estimation (\cref{chap:consensus.estimator}) is introduced and generalized (\cref{chap:consensus.privacy}) to avoid the disclosure of a node's private estimates and measurements. Although the literature is quite vast on consensus estimation, and especially maximum-value estimation has been extensively studied by several authors, to the best of our knowledge, our algorithm is the first to address dynamic maximum-value and minimum-value consensus explicitly. 

Even though single-objective real-time optimization via extremum-seeking-control and trajectory tracking of systems with unknown control direction via output-feedback are topics that have received much attention in the past three decades, we have also managed to provide some contributions to them in \cref{chap:siso,chap:siso.esc}. For both cases, we have extended the current theory on sliding-mode control via switching functions to admit chattering-free control laws by resorting to continuous switching elements. Furthermore, we generalize the sliding-mode controller such that the proposed control law covers several different types of sliding surfaces. Both of these contributions, but especially the latter, extend the possible practical implementations in the sense of expanding the design choices available to the control designer.

Additionally, we show that the proposed sliding-mode controllers for extremum-seeking and output tracking can be applied to arbitrary nonlinear systems, given that the closed-loop satisfy the conditions to apply Tikhonov's theorem.

Finally, throughout the manuscript, we have illustrated most of the proposed algorithms through numerical simulations, inspired by theory and from practical applications. Notably, in \cref{chap:raman}, an engineering application concerning the multi-variable real-time optimization for the output signal power spectrum of Raman optical amplifiers was considered to evaluate the performance of the multi-objective extremum-seeking control for Nash equilibrium seeking. The parameters of the optical fiber used in the simulations describe a Raman optical amplifier based on the {\em TrueWave}\textregistered {\em Reach - Low Water Peak}(RFLWP) optical fiber, with model parameters experimentally characterized by the company OFS Fitel Denmark ApS. The results are consistent with those of the literature, and performance improvements are obtained with the proposed scheme to pursue a flat signal spectrum and output signal power regulation.

\section{Future Works}
A common next step of every technique developed in this Thesis is experimental validation. Even though we thought out the simulations to display some practical implementation aspects, such as discrete-time implementation, control amplitude limitation, and chattering alleviation, experimental validation is indispensable to evaluate the proposed algorithms appropriately.

Regarding the multi-variable optimization algorithm proposed in \cref{chap:miso}, there are three future developments that could improve the algorithm significantly:
%
\begin{itemize}
    \item proof of convergence of the control design via unit-vector control, illustrated in \cref{chap:miso.numerical}, with control law~\eqref{eq:miso.control} rewritten as
    \begin{align}
        u_j(t) &= \rho(t) \frac{
            s(t)
        }{
            \max\left(
                \norm{s(t)} ,\, \epsilon
            \right)
        }
        \\
        s(t) &= \sin\left[
            \frac{\pi \sigma(t)}{T_j}
        \right]
    \end{align}
    %
    where we use $\max\left(\norm{s(t)} ,\, \epsilon\right)$ instead of $\norm{s(t)}$ to make the control law continuous;
    \item generalization to input-affine nonlinear systems;
    \item distributed implementation relying on scalarization via consensus estimation using average consensus or the proposed maximum (minimum) consensus estimator.
\end{itemize}
%
It is worth noting that many authors suggest the approximation of $\norm{v}$ using $\norm{v} + \epsilon$. However, we have experienced that this approach delivers worse results when compared to $\max\left(\norm{v} ,\, \epsilon\right)$. Naturally, more experimentation is necessary before drawing any conclusions.

%%%%

Extensively used in this Thesis and by many authors in the past three decades, the control design via periodically switching functions, $\sign(\sin(.))$, can still be significantly improved and generalized. Taking inspiration from the example of unit-vector control for the multi-variable case, we suggest the following generic periodically switching function:
%
\begin{align}
    u(t) &= \rho(t) \,\phi\! \left(
        \sin\left[
            \pi T^{-1} \sigma(t)
        \right]
    \right)
\end{align}
%
where $\phi(.)$ can be any control law based on the output error that is capable of making the origin of the closed-loop process asymptotically stable. For instance, if one whishes to apply super-twisting control, then
%
\begin{subequations}
\begin{align}
    \phi(s) &= \phi_1(s) + \int_0^t \phi_2(s)d\tau
    \\
    \phi_1(s) &= \left\{\begin{array}{lcl}
        -\kappa_1 \abs{s_0}^{0.5}\sign(s) &,& \abs{s} > s_0\\
        -\kappa_1 \abs{s}^{0.5}\sign(s) &,& \abs{s} \leq s_0
    \end{array}\right.
    \\
    \phi_2(s) &= \left\{\begin{array}{lcl}
        -\phi_M\\
        -\kappa_2 \sign(s) &,& \abs{\phi} \leq \phi_M
    \end{array}\right.
\end{align}
\end{subequations}
%
The original periodically switching function is recovered by selecting $\phi(.) = \sign(.)$.
%
Extensive studies must still be conducted to characterize $\phi(.)$ properly, and to develop the stability and convergence results.

%%%%

As discussed in \cref{chap:intro.similar.works}, the stabilization controller for SISO systems with unknown output sign can be related to \parencite{scheinker2013minimum,scheinker2016model}, where the authors provide general results based on the minimization of control Lyapunov functions. Such a formulation allows one to consider stabilization as a consequence of solving an optimization problem via extremum-seeking control. Following this same direction with the controller proposed in \cref{chap:siso.output.sign} might yield interesting new results. Also, one thing to keep in mind is that, when using control Lyapunov function, the objective function is known to the control designer, a fact that might be exploited to improve the performance of the sliding-mode controller.

%%%%

Regarding the dynamic consensus estimator, we have already indicated a modification to ensure privacy preservation in \cref{chap:consensus.privacy}. Although we have tested in simulations this modified version of the algorithm, its stability and convergence properties still need to be studied. The other developments in the proposed consensus algorithm that we see as the most promising are:
%
\begin{itemize}
    \item adapt the algorithm to enable a discrete-time implementation;
    \item improve the algorithm to ensure robustness to network delays;
    \item improve the algorithm to ensure robustness to malicious network attacks.
\end{itemize}
