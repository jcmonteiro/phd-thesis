TEX     = pdflatex
BIBTEX  = biber
MAKEIDX = makeindex

TEXFLAGS    = -halt-on-error -interaction=batchmode
BIBTEXFLAGS =
IDXFLAGS    = -q -s gind.ist
LSTFLAGS    = -q -s coppe.ist

_rm = rm -f

TEXFILE = main
OTHER_FILES := preamble.tex main.bib esc.bib coppe.cls tables/esc.tex tables/esc_multi.tex tables/thesis.tex
CHAPTER_FILES := $(shell find `pwd` -maxdepth 2 -name 'chap_*.tex')
PICTURES := $(shell find `pwd`/figs -name '*.pdf')

default:
	@echo "\n============= IMAGE FILES ============="
	$(MAKE) images -i
	@echo "=======================================\n"
	@echo "\n============== MAIN FILE =============="
	$(MAKE) main.pdf
	@echo "=======================================\n"

.SUFFIXES: .tex

$(TEXFILE).pdf: $(TEXFILE).tex $(CHAPTER_FILES) $(OTHER_FILES) $(PICTURES)
	$(TEX) $(TEXFLAGS) -draftmode $<
	$(eval name := $(basename $<))
	if grep -s "There were undefined references" $(name).log; then \
	  $(BIBTEX) $(BIBTEXFLAGS) $(name); \
	fi
	if grep -s '$(name).idx' $(name).log; then \
	  $(MAKEIDX) $(IDXFLAGS) $(name).idx -o $(name).ind;\
	fi
	if grep -s '$(name).syx' $(name).log; then \
	  $(MAKEIDX) $(LSTFLAGS) $(name).syx -o $(name).los;\
	fi
	if grep -s '$(name).abx' $(name).log; then \
	  $(MAKEIDX) $(LSTFLAGS) $(name).abx -o $(name).lab;\
	fi
	$(TEX) $(TEXFLAGS) --synctex=1 $<
	count=1 ;\
	while grep -s "Rerun to get cross-references right" $(name).log && [ $$count -le 3 ] ; do \
	  $(TEX) $(TEXFLAGS) --synctex=1 $< ;\
	  count=`expr $$count + 1` ;\
	done
	@echo ""

images:
	cd figs/tikz && $(MAKE)
	cd figs/sim && $(MAKE)

preamble:
	pdftex -ini -jobname="compiled_preamble" "&pdflatex" mylatexformat.ltx $(TEXFILE).tex

.PHONY: clean 

clean:
	$(_rm) *.aux *.idx *.blg *.bbl *.glo *.bz2 *.toc *.lof *.lot \
	       *.syx *.abx *.lab *.ilg *.los *.ind *.gls *.out *~
