\chapter{Mathematical Background}
\label{chap:math.background}

\section{Notation}

\begin{itemize}
    \item $\realsE = \reals \cup \{-\infty, +\infty\}$ is the set of
    extended real numbers, $\preals = \{x\in\reals | x > 0\}$, and
    $\prealsE = \preals \cup \{+\infty\}$.
    \symbl{$\reals$}{The set of real numbers}%
    \symbl{$\preals$}{The set of positive real numbers}%
    \symbl{$\realsE$}{The set of extended real numbers}%
    \symbl{$\prealsE$}{The set of positive extended real numbers}%
    %
    \item The Euclidean norm of a vector $x \in \reals^n$ is denoted $\norm{x}$.
    %
    \item The $i$-th element of a vector $x \in \reals^n$ is denoted either
    $x_i$ or $(x)_i$.
    %
    \item The absolute value $\abs{x}$ of a vector $x \in \reals^n$ is taken
    element-wise, such that $\abs{x} = \transp{[\abs{x_1}\ \abs{x_2} \hdots
    \abs{x_n}]}$
    %
    \item The sign function is defined as
    \begin{align}
    \sign_\alpha(x) = \left\{
    \begin{array}{rcl}
        +1 &,& x > 0 \\
        -1 &,& x < 0 \\
         0 &,& x = 0
    \end{array}
    \right.
    \end{align}
    %
    \item The geometric sum of two sets $M_1,M_2 \subseteq \reals^n$ is denoted
    by ``+'',
    \begin{align}
    M_1 + M_2 = \bigcup \{x_1+x_2\}\ ,\ x_1 \in M_1\ ,\ x_2 \in M_2
    \end{align}
    %
    \item The set of continuous functions defined on $\Omega\subseteq\reals^n$,
    continuously differentiable up to order $k$ is denoted $C^k(\Omega)$. If
    $\Omega = \reals^n$, the domain is omitted and $C^k(\Omega) = C^k$.
    \symbl{$\classC{k}(\Omega)$}{The set of continuously differentiable
    functions up to order $k$ defined on $\Omega\subseteq\reals^n$}%
    %
    \item If $V : \reals^n \mapsto \reals$ belongs to $C^1$, its gradient is defined
    as the column vector $\nabla V(x) =
    \transp{\left[%
    \frac{\partial V}{\partial x_1}\ %
    \frac{\partial V}{\partial x_2}\ %
    \hdots\ %
    \frac{\partial V}{\partial x_n}\right]}$. If $\sigma : \reals^n \mapsto
    \reals^p$, its gradient is a matrix with columns given by the gradients of
    its components $\nabla \sigma(x) =
    \transp{\left[%
    \nabla \sigma_1(x)\ \nabla \sigma_2(x)\ \hdots\ \nabla \sigma_p(x)
    \right]}$.
    %
    \item The power set of $M$, set of all possible combinations of subsets
    of $M$, is denoted $2^M$.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% | SECTION | %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% +---------+ %%%%%%%%%%%%%%%%%%%%%%%
\section{Discontinuous Systems}
The mathematical foundations for SMC lie in Filippov's theory of ordinary
differential equations with discontinuous right-hand side
\parencite{filippov1988differential}, henceforth called discontinuous ODEs.
%
It is known that Filippov's theory fails to describe some systems nonlinear
in the control input.
%
Even though we deal with systems affine in the control input, for the sake of
completeness, two other interpretations of solutions to discontinuous ODEs are
considered.
%
The first one is due to Utkin's equivalent control method
\parencite{utkin1992sliding}, while the other is an extension of Filippov's
theory with elements taken from Utkin's method
\parencite{aizerman1974foundations}.

%%%%

For continuous ODEs (those with continuous right-hand sides), the classical
theory~\parencite{chicone2006ordinary} introduces a solution to
%
\begin{align}\label{eq:ode.classical}
\dot{x} = f(t, x)\ \ ,\ \ f : \reals \times \reals^n \mapsto \reals^n
\end{align}
%
as a differentiable function $x : \reals \mapsto \reals^n$, defined on some
interval $\interval \subseteq \reals$.
%
Furthermore, if $f \in C^k$, for some $k = 1,2,\hdots,\infty$, then also
$x \in C^k$.

%%%%

Existence and uniqueness of solutions of~\eqref{eq:ode.classical} are properties
usually related to smoothness or Lipschitz continuity, with Lipschitz continuity
observed with respect to the second argument of $f$.
%
Unfortunately, neither smoothness nor Lipschitz continuity are properties
that apply to discontinuous ODEs.

%%%%

\begin{example}
Consider the simple relay controlled integrator
%
\begin{align}
\dot{x} = -\sign(x)
\end{align}
%
This trivial example of great practical relevance cannot be studied in light of
classical ODE theory, since the sign function is discontinuous at $x=0$.
\end{example}

%%%%

Instead, we consider $f$ in \eqref{eq:ode.classical} piece-wise continuous,
such that its domain of definition can be split into $N$ disjoint open connected
subsets $G_j \subset \reals\times\reals^n$, and the boundary set
$S = \bigcup\limits_{j=1}^{N} \partial G_j$ is of measure zero (in Lebesgue's sense).
%
The function $f$ is continuous at every $G_j$ and for each $(t^*,x^*) \in
\partial G_j$, any sequence $(t^k,x^k) \in G_j:(t^k,x^k) \to (t^*,x^*)$
is such that $f(t^k,x^k) \to f^j(t^*,x^*)$.
%
Functions $f_j : \reals \times \reals^n \mapsto \reals^n$ are defined according
to this limiting process,
%
\begin{align}\label{eq:filippov.fj}
f^j(t,x) = \lim_{(t^k,x^k) \to (t^*,x^*)} f(t^k,x^k)\ \ ,\ %
(t^k,x^k) \in G_j\ \ ,\ \ %
(t^*,x^*) \in \partial G_j
\end{align}

%%%%

\subsection{Filippov's Definition of Solutions}
Filippov's theory of ODEs with discontinuous right-hand side provides an
\textit{axiomatic} definition of solutions to~\eqref{eq:ode.classical} with
$f$ piecewise continuous%
\footnote{Actually, Filippov's theory covers the case where $f(t,x)$ is not
piecewise continuous, but locally measurable. For such systems, the method
of Filippov regularization~\parencite[page 85]{filippov1988differential}
is used.}.
%
For that, equation~\eqref{eq:ode.classical} is substituted by the following
differential inclusion
%
\begin{subequations}
\label{eq:di.filipov}
\begin{align}
&\dot{x} \in K_{f}(t,x),
\\
&K_f(t,x) = \left\{
\begin{array}{ccl}
    \{f(t,x)\}
        &,&
        (t,x) \in \reals^{n+1} \setminus S
    \\
    \mathrm{co} \left( \underset{j \in \mathcal{N}(t,x)}{\bigcup} \{f^j(t,x)\} \right)
        &,&
        (t,x) \in S
\end{array}
\right.
\end{align}
\end{subequations}
%
where $\mathrm{co}(M)$ is the convex closure of a set $M$, and set-valued index
function $\mathcal{N} : \reals^{n+1} \mapsto 2^{\{1,2,\hdots,N\}}$ defined on
$S$ indicates boundaries $\partial G_j$ which intersect at $(t,x)$,
%
\begin{align}
\mathcal{N}(t,x) = \{ j \in \{1,2,\hdots,N\}\ |\ (t,x) \in \partial G_j \}
\end{align}
%
Note that, by definition, the set $K_f(t,x)$ is a convex polyhedron for
$(t,x) \in S$.

%%%%

\begin{definition}[\cite{filippov1988differential}, page 50]
An absolutely continuous function $x : \mathcal{I} \mapsto \reals^n$ defined
on some interval or segment $\mathcal{I}$ is a
solution of~\eqref{eq:ode.classical} if it satisfies the differential
inclusion~\eqref{eq:di.filipov} almost everywhere on $\mathcal{I}$%
\footnote{The term almost everywhere is used because there might be points $t
\in \mathcal{I}$, constituting a set of zero measure, for which $x$ is not
differentiable.}.
\end{definition}

%%%%

The intuition behind Filippov's definition is actually simple.
%
The set-valued function $K_f$ determines allowable velocities that a solution
of~\eqref{eq:ode.classical} might take.
%
For a point $(t,x) \not\in S$ that does not belong to the discontinuity set
(switching manifold in the case of SMC), $K_f(t,x)$ is a singleton containing
only $f(t,x)$, such that $\dot{x} = f(t,x)$.
%
For $(t,x) \in S$ in the discontinuity set, the velocity should belong to the
convex combination of every possible velocity vector in a neighborhood
$B_\delta \setminus S$ $(\delta \to 0)$ of $(t,x)$, that is, an infinitesimal
neighborhood of $(t,x)$ that discards the set $S$ of zero measure.

%%%%

For SMC, it is common to consider $f(t,x)$ discontinuous only on a smooth
manifold $S = \{x \in \reals^n\ |\ \sigma(x)=0\}$ computed from the switching
function $\sigma : \reals^n \mapsto \reals$, which separates the state-space into
two domains $G^+ = \{x \in \reals^n\ |\ \sigma(x) > 0\}$ and
$G^- = \{x \in \reals^n\ |\ \sigma(x) < 0\}$, as shown in
\cref{fig:switching.filippov}.

%%%%

\begin{figure}[h]
\centering
\includegraphics[scale=1]{figs/tikz/switching_smooth_filippov}
\caption{Geometric illustration of Filippov's definition with $f$ discontinuous
on a smooth manifold $S = \{x \in \reals^n\ |\ \sigma(x)=0\}$.}
\label{fig:switching.filippov}
\end{figure}

%%%%

Let functions $f^+$ and $f^-$ be defined on $G^+$ and $G^-$ by the limiting
process~\eqref{eq:filippov.fj}, for a constant $t$.
%
In this case, the set $K_f$ obtained at $(t,x) \in S$ is a line segment
connecting the end points of vectors $f^+$ and $f^-$,
%
\begin{align}\label{eq:kf.smooth}
K_f(t, x) = \{ v \in \reals^n \ |\ v = \alpha f^- + (1-\alpha) f^+\}
\ \ ,\ \ %
(t,x) \in S
\ \ ,\ \ %
\alpha \in [0,1]
\end{align}
%
In order to obtain a smooth movement along the manifold, while satisfying
the inclusion~\eqref{eq:di.filipov}, the velocity vector $f_0$ at
$(t,x) \in S$ should belong to the tangent space $T_x S$ of $S$ at $(t,x)$.
%
Since $T_x S = \{v \in \reals^n\ |\ \inner{\nabla \sigma(x)}{v} = 0\}$, the
function $f_0$ is such that $\inner{\nabla \sigma(x)}{f_0} = 0$, with
$f_0 \in K_f(t,x)$ from~\eqref{eq:kf.smooth}.
%
This equation can always be satisfied if $f_N^+ f_N^- < 0$ at $(t,x)$, where
$f_N^+ = \inner{\nabla \sigma}{f^+}$ and $f_N^- = \inner{\nabla \sigma}{f^-}$
are the length of the projections of vectors $f_N^+(t,x)$ and $f_N^-(t,x)$ onto
$\nabla \sigma (x)$.

%%%%

\begin{definition}\label{def:filippov.smooth}
An absolutely continuous function $x : \mathcal{I} \mapsto \reals^n$ defined
on some interval or segment $\mathcal{I}$ is a
solution of~\eqref{eq:ode.classical} with $f$ discontinuous only on a smooth
manifold $S = \{x \in \reals^n\ |\ \sigma(x)=0\}$ and $f_N^+ f_N^- < 0$ for
every $(t, x)$ if it satisfies
%
\begin{subequations}
\begin{align}
&\dot{x} = \left\{\begin{array}{ccl}
    f(t,x)
        &,&
        x \in \reals^{n} \setminus S
    \\
    f_0(t,x)
        &,&
        x \in S
\end{array}\right.
\\[0.2ex]
\label{eq:f0}
&f_0 = \frac{
    f_N^-f^+ - f_N^+f^-
}{
    f_N^- - f_N^+
}
\end{align}
\end{subequations}
%
almost everywhere on $\mathcal{I}$.
\end{definition}

%%%%

\begin{definition}
A solution $x : \mathcal{I} \mapsto \reals^n$ of~\eqref{eq:ode.classical} in
the sense of \cref{def:filippov.smooth} is said to slide on $S$, according
to the sliding motion equation~\eqref{eq:f0} after it reaches the manifold $S$.
\end{definition}

%%%%

\begin{definition}
If, for a fixed initial condition $x_0$, all solutions approach the manifold $S$
and reach it in some finite time $t \in \mathcal{I}$, these solution are said to
constitute a sliding family on $S$.
\end{definition}

%%%%

\Cref{def:filippov.smooth} assumes $f_N^+ f_N^- < 0$ at $(t,x)$, otherwise
$K_f(t,x) \cap T_xS = \emptyset$ and every possible solution satisfying~%
\eqref{eq:di.filipov} will cross $S$ at $(t,x)$.
%
In other words, if on both sides of the switching manifold $S$ the vector field
points to the same side of $S$, then trajectories necessarily cross the
manifold at $(t,x)$.

%%%%

\subsection{Nonlinear Systems Affine in the Control Input}
In feedback control, it is usual to deal with systems governed by continuous
dynamics, but forced by a (possibly) piecewise continuous input.
%
To represent this class of systems, let
%
\begin{align}\label{eq:ode.control}
\dot{x} = f(t,x,u(x))
\end{align}
%
be a modification of~\eqref{eq:ode.classical}, with $f:\reals^{n+1} \times
\reals^p \mapsto \reals^n$ continuous and control input $u : \reals^{n} \mapsto
\reals^p$ piecewise continuous.
%
Similar to \cref{def:filippov.smooth}, each component $u_i$ of $u$ is
assumed to be discontinuous only on a smooth surface
$S_i = \{x \in \reals^n\ |\ \sigma_i(x)=0\}$.
%
Furthermore, \eqref{eq:ode.control} is simplified to
%
\begin{align}\label{eq:ode.affine}
\dot{x} = a(t,x) + b(t,x)u(x)
\end{align}
%
with $a : \reals^{n+1} \mapsto \reals^n$ and $b : \reals^{n+1} \mapsto
\reals^{n \times p}$ continuous.
%
Although simpler than~\eqref{eq:ode.classical} and \eqref{eq:ode.control},
system~\eqref{eq:ode.affine} covers most practical engineering problems.
%
This modification changes the differential inclusion~\eqref{eq:di.filipov} to
%
\begin{subequations}
\label{eq:di.filipov.affine}
\begin{align}
&\dot{x} \in a(t,x) + b(t,x) K_{u}(x),
\\
\label{eq:ku}
&K_{u}(x) = \transp{\begin{bmatrix}K_{u_1}(x) &\hdots& K_{u_2}(x)\end{bmatrix}}
\\
\label{eq:ku.elements}
&K_{u_i}(x) = \left\{
\begin{array}{ccl}
    \{u_i(x)\}
        &,&
        \sigma_i(x) \neq 0
    \\
    \alpha u_i^-(x) + (1-\alpha)u_i^+(x)
        &,&
        \sigma_i(x) = 0
\end{array}
\right.
\end{align}
\end{subequations}
%
with $\alpha \in [0,1]$, such that $\alpha u_i^-(x) + (1-\alpha)u_i^+(x)$ is
the convex combination of $u_i^+(x)$ and $u_i^-(x)$, defined by the limiting
process~\eqref{eq:filippov.fj}.

%%%%

\subsection{Nonuniqueness of Solutions}
\Cref{def:filippov.smooth} guarantees the existence of solutions of%
~\eqref{eq:ode.classical}, but it does not state anything about uniqueness.
%
In fact, by definition of~\eqref{eq:di.filipov} on the switching manifold,
uniqueness of solutions of~\eqref{eq:ode.classical} is not expected.
%
Therefore, for control purposes, a control law should guarantee strong
stability properties, since these properties apply for every solution.
%
In this direction, the next section presents strong stability properties
based on Lyapunov's stability theory~\parencite{lyapunov1992thesis} and
its generalizations for discontinuous systems satisfying~\eqref{eq:di.filipov}.

%%%%

\subsection{Lyapunov Stability and Convergence Properties}
Stability is the kernel of most control problems and its modern definition is
due to the famous thesis of \cite{lyapunov1992thesis}, originally published in
Russian in 1892.
%
Prior to Lyapunov's work, stability analysis consisted of neglecting all terms
in $f$ of order higher than $k$ in $x$\footnote{%
In analyzing stability, it is usual to consider a time-invariant system and
augment the state vector $x$ by the fictitious state
$x_{n+1} = t$, with $\dot{x}_{n+1} = 1$, if the system is time-variant.},
usually by applying some linearization procedure, such as Taylor series
expansion.
%
One exception to this methodology is the work of Poincare, which inspired
some results presented by Lyapunov in his thesis.
%
In contrast to linearization and similar techniques, Lyapunov stability studies
how small deviations from the initial condition $x_0$ of a nominal trajectory
(solution) $x^*(t,t_0,x_0)$ impact the resulting motion.
%
That is, what happens to $x^*(t,t_0,x'_0)$ when one changes the initial
condition to $x'_0 \in x_0 + \ball{\delta}$.

%%%%

As commonly done in stability analysis, we study stability with respect to the
zero solution (origin) $x^*(t,t_0,x_0)$, since any problem can be put in this
perspective by an appropriate change of variables $z = x - x^*$.
%
The definitions of Lyapunov stability, asymptotic stability, exponential
stability and related properties are not given here, since they are the same
as the ones for continuous systems.
%
The reader is directed to \parencite{khalil02} for these definitions.
%
Roughly speaking, for an arbitrary vicinity $\ball{\epsilon}$ of the origin,
if one is able to find $B(\delta) : x_0 \in \ball{\delta}$ for which
trajectories do not leave $\ball{\epsilon}$, then the origin is stable.
%
Additionally, asymptotic stability implies that, on top of stability,
trajectories eventually reach the origin, while exponential stability forces
the convergence rate to be at least exponential.

%%%%

To redefine Lyapunov's function method for systems with discontinuous right-hand
side, we first revisit the classical method for $f$ continuous.
%
In this case, let a \textit{continuous function} $V \in \classC{0}$ represent,
in some sense, the system energy, with $V(x) > 0$ for $x\neq0$, and $V(0) = 0$.
%
If, for every solution $x(t)$ of~\eqref{eq:ode.classical}, the function
$V(x(t))$ is weakly decreasing, i.e. it is allowed to remain constant, then
the origin of~\eqref{eq:ode.classical} is stable.
%
Additionally, if $V(x(t))$ is strictly decreasing and tending to zero as
$t \to +\infty$, then the origin is asymptotically stable.
%
For $V \in \classC{1}$ \textit{continuously differentiable} the above properties
are rewritten in the widely used forms
%
\begin{subequations}
\begin{align}
\dot{V}(x) = \transp{\nabla} V(x) f(t,x) \leq 0 && \text{stable origin}
\\
\dot{V}(x) = \transp{\nabla} V(x) f(t,x)   <  0 && \text{asymptotically stable origin}
\end{align}
\end{subequations}

%%%%

By analyzing these stability conditions for continuous systems, it is clear
what must be tackled to obtain an equivalent method for discontinuous systems:
%
\begin{itemize}
    \item Even if $V$ is differentiable with respect to the state $x$, it will
    not be continuously differentiable, since $f(t,x)$ is piecewise continuous.
    Therefore, there is set of measure zero for which $\dot{V}$ is
    \textit{discontinuous}.
    %
    \item When the energy function $V$ is not everywhere differentiable,
    independent of the continuity or not of $f$, there is a set of measure zero
    for which $\dot{V}$ is \textit{undefined}.
\end{itemize}
%
In the first case, $V$ is differentiable with respect to $x$, $\dot{V}$ is
discontinuous at points $x \in S$ that belong to the switching manifold.
%
When $V$ is not everywhere differentiable, its derivative $\dot V$ is (usually)
undefined only at $x=0$.
%
Therefore, we consider generalized derivatives and generalized gradients, and
show how these concepts fit to non-smooth stability analysis\footnote{These
concepts are presented in a greater extent in~%
\parencite{polyakov2014stability}.}.

%%%%

\begin{definition}
Let $\varphi : \reals \mapsto \reals$ be a real-valued function and $\{h_n\}$ an
infinite sequence converging to zero.
%
A number
%
\begin{align}
D_{h_n}\varphi(t) = \lim_{n \to +\infty} \frac{
    \varphi(t + h_n) - \varphi(t)
}{
    h_n
}
\ \ ,\ \ %
D_{h_n}\varphi(t) \in \realsE
\end{align}
%
is called a derivative number.
%
The set of every possible derivative number
%
\begin{align}
\contingent{K}{\varphi(t)} = \bigcup\limits_{\{h_n\} \in \mathbb{K}} D_{h_n}\varphi(t)
\ \ ,\ \ %
\contingent{K}{\varphi(t)} \subset \ \realsE
\end{align}
%
with $\mathbb{K}$ the set of sequences $\{h_n\}$ converging to zero,
is called the contingent derivative set.
\end{definition}
%
Naturally, at points $t$ where $\varphi$ is differentiable, it must follow that
$\contingent{K}{\varphi(t)} = \{\dot \varphi (t)\}$.

%%%%

\begin{example}\label{ex:contingent.abs}
Let $\varphi(t) = \abs{t}$.
%
The contingent derivative at $t=0$ may be obtained by taking any sequence
$\{h_n^-\}$ converging to zero from the left
%
\begin{align}
D_{h_n^-}\varphi(0) = \lim_{n \to +\infty} \frac{
    \varphi(0 + h_n^-) - \varphi(0)
}{
    h_n^-
}
=
\lim_{n \to +\infty}
\frac{
    -h_n^-
}{
    h_n^-
}
=
-1
\end{align}
%
and any sequence $\{h_n^+\}$ converging to zero from the right
%
\begin{align}
D_{h_n^+}\varphi(0) = \lim_{n \to +\infty} \frac{
    \varphi(0 + h_n^+) - \varphi(0)
}{
    h_n^+
}
=
\lim_{n \to +\infty}
\frac{
    h_n^+
}{
    h_n^+
}
=
1
\end{align}
%
such that $\contingent{K}{\varphi(0)} = \{-1,1\}$.
\end{example}

%%%%

\begin{example}
Let $\varphi(t) = t \sin(1/t)$ for $t \neq 0$ and $\varphi(0) = 0$.
%
To find the contingent derivative at $t=0$ the process is more involved than
that of \cref{ex:contingent.abs}.
%
Considering any sequence $\{h_n\}$ converging to zero,
%
\begin{align}
D_{h_n} \varphi(0) = \lim_{n \to +\infty} \frac{
    \varphi(0 + h_n) - \varphi(0)
}{
    h_n
}
    = \lim_{n \to +\infty} \frac{ \varphi(h_n) }{ h_n }
    = \lim_{n \to +\infty} \sin(1/h_n)
\end{align}
%
and with $h_n = 1/(2\pi n + \alpha)$, $\alpha \in [-\pi/2, \pi/2]$
%
\begin{align}
D_{h_n} \varphi(0) = \lim_{n \to +\infty} \sin(2\pi n + \alpha)
    = \sin(\alpha)
\end{align}
%
such that the contingent derivative is the interval
$D_\mathbb{K} \varphi(0) = [-1, 1]$.
\end{example}

%%%%

Let $\mathcal{I} \subseteq \realsE$ be an interval possibly containing
$\pm \infty$.
%
A function $\varphi : \mathcal{I} \mapsto \realsE$ is decreasing on
$\mathcal{I}$ if and only if
%
\begin{align}
    \varphi(t_1) \leq \varphi(t_2)
    \ \ ,\ \ %
    \forall t_1 < t_2
\end{align}

%%%%

\begin{lemma}\label{lem:decreasing}
Let $\varphi : \reals \mapsto \reals$ be defined on some interval $\mathcal I$.
%
If $\contingent{K}{\varphi(t)} \leq 0$ on $\mathcal I$, then $\varphi$ is
decreasing on $\mathcal I$ and differentiable almost everywhere on $\mathcal I$.
\end{lemma}

%%%%

This lemma provides a background on which to develop a discontinuous Lyapunov
function method.
%
Nonetheless, it is more general than needed, since no further assumption is made
on $\phi$.
%
Since Lyapunov function method is applied to nonnegative functions,
\cref{lem:decreasing} is rewritten considering such functions.

%%%%

\begin{lemma}
Let $V : \reals \mapsto \reals$ be nonnegative on an interval $\mathcal{I}$,
%
\begin{enumerate}\label{lem:decreasing.nonnegative}
    \item continuous at any $t \in \mathcal{I} : V(t) = 0$ and
    \item its contingent derivative $\contingent{K}{V(t)} \leq 0$ for
    $t \in \mathcal{I} : V(t) \neq 0$,
\end{enumerate}
%
then $V(t)$ is decreasing and almost everywhere differentiable on $\mathcal{I}$.
\end{lemma}

%%%%

Basically, \cref{lem:decreasing.nonnegative} states that $V(t)$ converges
asymptotically to zero and it is not allowed to leave the origin.
%
It is clear that function $V(t)$ will be used as a Lyapunov function.
%
For that, one still needs to define its derivative when evaluated along the
trajectories of~\eqref{eq:di.filipov.affine}, i.e. determine a chain rule to
differentiate $V(x)$.
%
The first step in this direction is the generalization of direction derivatives.

%%%%

\begin{definition}
Let $V : \reals^n \mapsto \reals$ be a real-valued function defined on an open
nonempty set $\Omega \subseteq \reals^n$ and $\{v_n\}$ an infinite sequence of
real vectors converging $d \in \reals^n$.
%
A number
%
\begin{align}
D_{\{h_n\},\{v_n\}} V(x) = \lim_{n \to +\infty} \frac{
    V(x + h_n v_n) - V(x)
}{
    h_n
}
\ \ ,\ \ %
D_{\{h_n\},\{v_n\}} V(x) \in \realsE
\end{align}
%
is called a directional derivative number.
%
The set of every possible directional derivative number
%
\begin{align}
\dcontingent{K}{M}{d}{V(x)} = \bigcup\limits_{
    \{h_n\} \in \mathbb{K},
    \{v_n\} \in \mathbb{M}(d)
} D_{\{h_n\},\{v_n\}} V(x)
\ \ ,\ \ %
\dcontingent{K}{M}{d}{V(x)} \subset \realsE
\end{align}
%
with $\mathbb{M}(d)$ the set of sequences $\{v_n\}$ converging to $d$, is called
the contingent directional derivative set.
\end{definition}

%%%%

As observed for contingent derivatives, at points $x$ where $V$ is
differentiable, it follows that $\dcontingent{K}{M}{d}{V(x)} =
\{\transp{\nabla V(x)}d\}$.

%%%%

Finally, considering the derivative of $V(x)$ along the trajectories of $x$,
described by the differential inclusion~\eqref{eq:di.filipov.affine}, let
%
\begin{align}
    D_{K_f(t,x)} V(x) = \bigcup\limits_{d \in K_f(t,x)}
        \dcontingent{K}{M}{d}{V(x)}
\end{align}

%%%%

\begin{lemma}[\cite{polyakov2014stability}, corollary 2]
Let the set-valued function $K_f : \reals^{n+1} \mapsto 2^{\reals^n}$ in~%
\eqref{eq:di.filipov} be defined and upper-semicontinuous on $\interval \times
\Omega$ and set $K_f(t,x)$ be nonempty, compact and convex on this domain,
where $\Omega \subseteq \reals^n$ is open and nonempty.
%
Furthermore, let $V : \reals^n \mapsto \reals$ be a nonnegative function on
$\Omega$.
%
If the inequality
%
\begin{align}
D_{K_f(t,x)} V(x) \leq 0
\end{align}
holds for every $t \in \interval$ and $x \in \Omega : V(x) \neq 0$, then
function $V$ is decreasing along the solutions of~\eqref{eq:di.filipov}.
\end{lemma}

%%%%

The above lemma closely relates to Lyapunov's stability theory of dynamical
systems.
%
Nonetheless, before providing a functional approach to Lyapunov stability two
definitions are in order.

%%%%

\begin{definition}
A continuous function $W : \reals^n \mapsto \reals$ is said to be positive
definite iff $W(0)=0$ and $W(x)>0$ for every $x \in \reals^n \setminus \{0\}$.
\end{definition}

%%%%

\begin{definition}
A function $V : \reals^n \mapsto \reals$ defined on a nonempty set
$\Omega \subseteq \reals^n : \{0\} \in \mathrm{int}(\Omega)$ is said to be
proper on $\Omega$ iff
%
\begin{itemize}
    \item it is continuous at the origin;
    \item it is bounded by a positive definite function $\underbar{V} : \reals^n
    \mapsto \reals$ such that $\underbar{V}(x) \leq V(x)$.
\end{itemize}

%%%%

Furthermore, if $\Omega = \reals^n$ and $\underbar{V}$ is radially unbounded,
$V$ is globally proper.
\end{definition}

%%%%

\begin{theorem}[\cite{polyakov2014stability}, theorem 7]
\label{thm:stable}
Let a function $V : \reals^n \mapsto \reals$ be proper on $\Omega$ and
%
\begin{align}
    D_{K_f(t,x)} V(x) \leq 0
    \quad \mathrm{for} \quad
    t \in \reals\ \mathrm{and}\ x \in \Omega \setminus \{0\}
\end{align}
%
Then, the origin of system~\eqref{eq:di.filipov} is Lyapunov stable.
%
If $V$ is globally proper, then the origin is globally stable.
\end{theorem}

%%%%

Note how \cref{thm:stable} is closely related to usual definitions of Lyapunov
stability of continuous systems~\parencite{khalil02}.
%
Theorems for asymptotic, exponential and finite-time stability can be readily
stated by following the notion of contingent directional derivatives and
these properties continuous counterparts~\parencite{polyakov2014stability}.

%%%%

In the design of sliding-mode control laws, one usually defines a piecewise
continuous control input $u : \reals^n \mapsto \reals^p$, where each component
$u_i$ is discontinuous only on a smooth surface $S_i = \{x \in \reals^n
\ |\ \sigma_i(x)=0\}$.
%
This was discussed in page \pageref{eq:ode.affine}.
%
For such systems, considering a Lyapunov function $V : \reals^p \mapsto \reals$
defined on $\Omega_\sigma \subseteq \reals^p$, continuous at the origin and
differentiable at $\sigma \in \Omega_\sigma \setminus \{0\}$, it follows that
%
\begin{align}
    D_{K_f(t,x)} V(\sigma) = \transp{\nabla V(\sigma)} \dot{\sigma}
    \quad \mathrm{for} \quad
    \sigma \in \Omega_\sigma \setminus \{0\}
\end{align}

%%%%

\begin{corollary}\label{cor:lyapunov.smc}
Consider system~\eqref{eq:di.filipov.affine}, with control input $u$
discontinuous only on a smooth surface $S_i = \{x \in \reals^n\ |\ \sigma_i(x)
= 0\}$.
%
Let $\sigma$ denote the new coordinate system after the change of variables
$\sigma(x)$.
%
Furthermore, consider $V : \reals^p \mapsto \reals$ defined on $\Omega_\sigma
\subseteq \reals^p$, positive definite and differentiable at every $\sigma \in
\Omega_\sigma \setminus \{0\}$.
%
Then, the origin of the differential inclusion~\eqref{eq:di.filipov.affine} is
Lyapunov stable iff
%
\begin{align}
    \transp{\nabla V(\sigma)} \dot{\sigma} \leq 0
    \quad \mathrm{for} \quad
    \sigma \in \Omega_\sigma \setminus \{0\}
\end{align}
%
If $V$ is radially unbounded and $\Omega_\sigma = \reals^p$, then the origin is
globally stable.
\end{corollary}

%%%%

This corollary establishes circumstances for which a discontinuous system
stability may be inferred from traditional Lyapunov stability analysis.
%
This results is implicitly used in most of the SMC literature.

\subsection{Discussion on Definitions other than Filippov's}
Filippov's theory was criticized by many authors, mainly because it apparently
yields results not adequate to those observed in real application.
%
This happens when the system is nonlinear in the control input, but it is
arguable if these inconsistencies are not a consequence of oversimplified
models.

%%%%

Utkin provides an alternate definition, based on the concept of equivalent
control~\parencite{utkin1992sliding}.
%
To apply the equivalent control method, one considers system~%
\eqref{eq:ode.control} and computes a equivalent control input $u_{eq}$ such
that trajectories stay on the switching manifold.
%
This is achieved by finding $u_{eq}$ that solves
%
\begin{align}\label{eq:equivalent.control.implicit}
\inner{\nabla \sigma(x)}{f(t,x,u_{eq})}\ = 0
\end{align}
%
This procedure is can be written in terms of the differential inclusion
%
\begin{align}\label{eq:di.utkin}
\dot{x} \in f(t, x, K_u(x))
\end{align}
%
with solution defined as in \cref{def:filippov.smooth} and $K_u(x)$ from~%
\eqref{eq:ku}-\eqref{eq:ku.elements}.
%
It is clear that, in general, Utkin's equivalent control method allows $\dot x$
to take values from a non-convex set $f(t, x, K_u(x))$. a fact that complicates
the analysis of the method~\parencite{polyakov2014stability}.

%%%%

A definition that, in a way, merges both Utkin's and Filippov's definitions is
given by \cite{aizerman1974foundations}.
%
In short, it consists of taking the convex closure of $f(t, x, K_u(x))$, such
that
%
\begin{align}\label{eq:di.aiz.pya}
\dot{x} \in \mathrm{co} \left\{ f_0(t,x), f(t, x, u_{eq}) \right\}
\end{align}
%
An important aspect of this definition is that, if some stability property is
obtained for~\eqref{eq:di.aiz.pya}, than the same property holds for both
Filippov's and Utkin's definitions~\parencite{polyakov2014stability}.

%%%%

It is not hard to show that the three definitions coincide when $f$ is affine
in the control input and the switching manifold $S$ depends solely on $x$,
in fact, they  yield the exact same differential inclusions.
